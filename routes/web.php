
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@showHomePage')
    ->name('homePage')
    ->middleware('categoryCheck');

Route::get('/login', 'LoginController@showLoginPage')
    ->middleware('loginCheck');

Route::post('/login', 'LoginController@attemptLogin');

Route::get('/logout', 'LoginController@logout');

Route::get('/signup', 'RegistrationController@showRegistrationPage');

Route::post('/signup', 'RegistrationController@addUser');

Route::get('/adminpage/view', 'AdminController@showAdminPage')
    ->name('adminpage')
    ->middleware('permission', 'loginRequire');

Route::get('/admin/show-data-table', 'AdminController@getShowsForDatatable');
Route::get('/admin/movie-data-table', 'AdminController@getMoviesForDatatable');
Route::get('/admin/theatre-data-table', 'AdminController@getTheatresForDatatable');

Route::post('/movie/create', 'AdminController@addMovie')
    ->name('storeMovie')
    ->middleware('permission', 'loginRequire');

Route::post('/movie/update', 'AdminController@editMovie')
    ->name('editMovie')->middleware('loginRequire');

Route::post('/movie/delete', 'AdminController@deleteMovie')
    ->middleware('loginRequire');

Route::post('/theatre/create', 'AdminController@addTheatre')
    ->name('storeTheatre')
    ->middleware('permission', 'loginRequire');

Route::post('/theatre/update', 'AdminController@editTheatre')
    ->name('editTheatre')->middleware('loginRequire');

Route::post('/theatre/delete', 'AdminController@deleteTheatre')
    ->middleware('loginRequire');

Route::get('error', function () {
    return 'Error Page';
});

Route::post('/seat/view', 'SeatController@showSeatPage')
    ->middleware('loginRequire');

Route::get('/adminpage/movie/modal/{id?}', 'AdminController@loadMovieModalForm');
Route::get('/adminpage/theatre/modal/{id?}', 'AdminController@loadTheatreModalForm');
Route::get('/adminpage/show/modal/{id?}', 'AdminController@loadShowModalForm');

Route::post('/show/create', 'AdminController@addShow')
    ->name('storeShow')->middleware('loginRequire');

Route::post('/show/update', 'AdminController@editShow')
    ->name('editShow')->middleware('loginRequire');

Route::post('/show/delete', 'AdminController@deleteShow')
    ->middleware('loginRequire');

Route::post('/load/screen', 'AdminController@loadScreens');

Route::get('/movie/details/{id}/{date}', 'HomeController@showMovie');

Route::post('/seat/update', 'SeatController@updateSeat')
    ->name('seatUpdate')->middleware('loginRequire');

Route::post('/seat/check', 'SeatController@checkSeat')
    ->name('seatCheck')->middleware('loginRequire');

Route::post('/seat/confirmation', 'SeatController@confirmation')
    ->name('confirmation')->middleware('loginRequire');

Route::get('/login/{provider}', 'LoginController@redirectToProvider');

Route::get('/login/{provider}/callback', 'LoginController@handleProviderCallback');

Route::get('/payment', 'PaymentController@paymentPage');

Route::post('/get/session', 'PaymentController@getSession')
    ->name('getSession');

Route::post('/pay', 'PaymentController@pay')
    ->name('payment')->middleware('loginRequire');