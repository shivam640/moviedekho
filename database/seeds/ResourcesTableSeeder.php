<?php

use Illuminate\Database\Seeder;
use App\Resource;

class ResourcesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Resource::firstOrCreate(['name' => 'adminpage']);
        Resource::firstOrCreate(['name' => 'movie']);
        Resource::firstOrCreate(['name' => 'theatre']);
        Resource::firstOrCreate(['name' => 'homepage']);
    }
}
