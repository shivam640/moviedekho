<?php

use Illuminate\Database\Seeder;
use App\RoleResourcePermission;

class Role_Resource_PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	RoleResourcePermission::firstOrCreate(['role_id' => '2', 'resource_id' => '3', 'permission_id' => '1']);
    	RoleResourcePermission::firstOrCreate(['role_id' => '2', 'resource_id' => '2', 'permission_id' => '1']);
    	RoleResourcePermission::firstOrCreate(['role_id' => '2', 'resource_id' => '4', 'permission_id' => '4']);
    	RoleResourcePermission::firstOrCreate(['role_id' => '2', 'resource_id' => '1', 'permission_id' => '4']);
    	RoleResourcePermission::firstOrCreate(['role_id' => '1', 'resource_id' => '4', 'permission_id' => '4']);
    }
}
