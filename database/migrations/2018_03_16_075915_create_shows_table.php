<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shows', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('screen_id')->unsigned();
            $table->foreign('screen_id')
                  ->references('id')->on('screens')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            $table->integer('movie_id')->unsigned();
            $table->foreign('movie_id')
                  ->references('id')->on('movies')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            $table->time('time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shows');
    }
}
