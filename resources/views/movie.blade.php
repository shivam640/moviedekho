@extends('layouts.master')

@section('title', 'moviedekho.com')

@section('css')
    {{ Html::style(mix("css/movie.css")) }}
@endsection

@section('navbar')
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle"
                        data-toggle="collapse" data-target="#nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"> Moviedekho.com</a>
            </div>
            <div  class="collapse navbar-collapse" id="nav-collapse">
                <div id="nav-right">
                    @if (Auth::check())
                        <ul class="nav navbar-nav">
                            <li><span id="username">
                            {{ "Hi, " . Auth::user()->name }}</span></li>
                            <li><a href="/logout">
                                <span class="glyphicon glyphicon-log-out">
                                </span> Log Out</a></li>
                        </ul>
                    @else
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="/login">
                                    <span class="glyphicon glyphicon-log-in">
                                    </span> Sign In
                                </a>
                            </li>
                        </ul>
                    @endif
                </div>
            </div>
        </div>
    </nav>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12" id="movie-section">
                <div id="image-box">
                    <img id="banner" src="/storage/app/{{ $movie->poster }}"
                         width=100% height="500px">
                    <div id="information">
                        <div id="movie-header">
                            <strong>{{ $movie->movie_name }}</strong>
                        </div>
                        <div id="left">
                            <strong>Director:</strong> {{ $movie->director }}
                            <br>
                            <strong>Casts:</strong> {{ $movie->cast }}
                        </div>
                        <div id="right">
                            <i class="fa fa-clock-o"></i> {{ $movie->duration }} mins
                        </div>
                    </div>
                </div>
                <div id="flip">
                    <strong>Description </strong>
                    <i class="fa fa-chevron-down"></i>
                </div>
                <div id="panel">{{ $movie->description }}</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div id="theatre-section">
                    <div class="row">
                        @foreach( $dates as $showDate)
                            @if (date('Y-m-d', strtotime($showDate)) === date
                            ('Y-m-d', strtotime($selectedDate)))
                                <a href="{{ date('Y-m-d', strtotime($showDate)) }}"
                                   class="date-box">
                                    <strong>
                                        {{ date('d D', strtotime($showDate)) }}
                                    </strong>
                                </a>
                            @else
                                <a href="{{ date('Y-m-d', strtotime($showDate))
                                }}">
                                    <strong>{{ date('d D', strtotime($showDate))
                            }}</strong>
                                </a>
                            @endif
                            &nbsp;
                        @endforeach
                    </div>
                    <div class="row">
                        @foreach( $shows as $show )
                            <hr>
                            <strong>{{ $show->theatre_name }}:</strong>
                            {{ $show->address }}
                            @foreach(explode(',', $show->times) as $time )
                                @php
                                    $showArray = explode('|', $time);
                                @endphp
                                @if (date_format($presentTime,"Y-m-d") === $selectedDate)
                                    @if (strtotime($showArray[1]) < strtotime($presentTime))
                                        <button type="button" disabled
                                                class="btn btn-default show-button">
                                            {{ date("h:i a", strtotime($showArray[1])) }}
                                        </button>
                                    @else
                                        <button type="button" id="{{ $showArray[0] }}"
                                                class="btn btn-info show-button">
                                            {{ date("h:i a", strtotime($showArray[1])) }}
                                        </button>
                                    @endif
                                @else
                                    <button type="button" id="{{ $showArray[0] }}"
                                            class="btn btn-info show-button">
                                        {{ date("h:i a", strtotime($showArray[1])) }}
                                    </button>
                                @endif
                            @endforeach
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        {{ Form::open([
            'url' => 'https://moviedekho.com/seat/view',
            'method' => 'post',
            'id' => 'form'
            ])
        }}
        {{ Form::text('showId', null, ['class' => 'hidden']) }}
        {{ Form::close() }}
    </div>
@endsection

@section('js')
    {{ Html::script(mix("js/movie.js")) }}
@endsection