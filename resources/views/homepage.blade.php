@extends('layouts.master')

@section('title', 'moviedekho.com')

@section('css')
<link rel="stylesheet"
      type="text/css"
      href="https://cdn.datatables.net/v/bs/dt-1.10.16/r-2.2.1/sc-1.4.4/datatables.min.css"/>
{{ Html::style(mix("css/homepage.css")) }}
@endsection

@section('navbar')
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle"
                    data-toggle="collapse" data-target="#nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"> Moviedekho.com</a>
        </div>
        <div  class="collapse navbar-collapse" id="nav-collapse">
            <div id="nav-right">
                @if (Auth::check())
                    <ul class="nav navbar-nav">
                        <li><span id="username">
                        {{ "Hi, " . Auth::user()->name }}
                        </span></li>
                        <li><a href="/logout">
                                <span class="glyphicon glyphicon-log-out">
                                </span> Log Out</a>
                        </li>
                    </ul>
                @else
                    <ul  class="nav navbar-nav">
                        <li>
                            <a href="/login">
                                <span class="glyphicon glyphicon-log-in"></span>
                                Sign In</a>
                        </li>
                        <li>
                            <a href="/signup">
                                <span class="glyphicon glyphicon-user"></span>
                                Sign Up</a>
                        </li>
                    </ul>
                @endif
            </div>
        </div>
    </div>
</nav>
@endsection

@section('content')
<div class="container">
    <div id="movie" class="carousel slide" data-ride="carousel">
        {{-- Indicators --}}
        <ol class="carousel-indicators">
            @foreach( $movies as $movie )
                <li data-target="#movie" 
                    data-slide-to="{{ $loop->index }}"
                    class="{{ $loop->first ? 'active' : '' }}"></li>
            @endforeach
        </ol>
        {{-- Wrapper for slides --}}
        <div class="carousel-inner" role="listbox">
            @foreach( $movies as $movie)
                <div class="item {{ $loop->first ? ' active' : '' }}" >
                    <img src="storage/app/{{ $movie->poster }}"
                         style="width:100%; height:100%;">
                </div>
            @endforeach
        </div>
        {{-- Controls --}}
        <a class="left carousel-control" href="#movie" role="button" 
           data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" 
                  aria-hidden="true">
            </span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#movie" role="button"
           data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" 
                  aria-hidden="true">
            </span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <div class="row">
        <table id="user-movie-table" class="table table-hover">
        </table>
    </div>
</div>
@endsection

@section('js')
{{-- CDN reference of datatables.js --}}
<script type="text/javascript"
        src="https://cdn.datatables.net/v/bs/dt-1.10.16/r-2.2.1/sc-1.4.4/datatables.min.js">
</script>
{{ Html::script(mix("js/homepage.js")) }}
@endsection
