@extends('layouts.master')

@section('title', 'moviedekho.com')

@section('css')
    {{ Html::style(mix("css/confirmation.css")) }}
@endsection

@section('navbar')
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="/"> Moviedekho.com</a>
                <input type="hidden" id="seat-update-url"
                       value="{{ route('seatUpdate') }}">
                <input type="hidden" id="seat-check-url"
                       value="{{ route('seatCheck') }}">
            </div>
            <div id="show-information">
                <ul class="nav navbar-nav">
                    <li><span id="movie">{{ $show->movie_name }}</span></li>
                    <li><span id="theatre">
                        {{ date("h:i a", strtotime($show->time)) }},
                        {{ date("d M", strtotime($show->date)) }}<br>
                        {{ $show->theatre_name }}:{{ $show->address }}
                    </span></li>
                </ul>
            </div>
        </div>
    </nav>
@endsection

@section('content')
    <div id="page">
        <div class="container">
            <div class="row col-xs-6" id="slip">
                <div id="info">
                    <h3>Booking Summary</h3>
                    <hr>
                    @php
                        $total = 0;
                        $count = 0;
                    @endphp
                    @foreach( explode(',', $seats) as $seat )
                        @php
                            $total += 100;
                            $count++;
                        @endphp
                    @endforeach
                    <p><strong>{{ $seats }} ({{ $count }} tickets)</strong></p>
                    <p id="screen">Screen {{ $show->screen_name }}</p>
                    <input type="hidden" id="id" value="{{ $id }}">
                    <input type="hidden" id="seats" value="{{ $seats }}">
                </div>
                <div id="amount">
                    <p id="total"><strong>Total Amount &nbsp; Rs.{{ $total
                    }}</strong></p>
                </div>
            </div>
            <div class="row col-xs-12">
                <a class="btn btn-success" role="button" id="proceed">Proceed to pay</a>
            </div>
        </div>
    </div>
@endsection

@section('js')
    {{-- CDN reference of Bootbox.js --}}
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js">
    </script>
    {{ Html::script(mix("js/confirmation.js")) }}
@endsection