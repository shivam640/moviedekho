<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h2><strong>Welcome to moviedekho</strong></h2>
    <hr>
    <p><strong>Theatre:</strong> {{ $theatreName }}</p>
    <p><strong>Address:</strong>{{ $address }}</p>
    <p><strong>Screen:</strong> {{ $screenName }}</p>
    <p><strong>Movie Name:</strong> {{ $movieName }}</p>
    <p><strong>Show Date:</strong>{{ $showDate }}</p>
    <p><strong>Show Time:</strong>{{ $showTime }}</p>
    <p><strong>Booked by:</strong>{{ $userName }}</p>
    <p><strong>Email Id:</strong>{{ $emailId }}</p>
    <p><strong>Seats:</strong>{{ $seats }}</p>
</body>
</html>