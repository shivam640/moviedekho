@extends('layouts.master')

@section('title', 'moviedekho.com')

@section('css')
{{ Html::style(mix("css/login.css")) }}
@endsection

@section('content')
<div class="text-center">
    <form class="form-signin" method="POST" action="/login">
        @csrf
        <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
        <hr>
        <a id="facebook" href="{{ url('/login/facebook') }}"
           class="btn btn-block btn-social btn-facebook">
            <i class="fa fa-facebook"></i> Continue with Facebook
        </a>
        <a id="google" href="{{ url('/login/google') }}"
           class="btn btn-google">
            <i class="fa fa-google"></i> Continue with Google
        </a>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" id="inputEmail" class="form-control"
               placeholder="Email address" name="email" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control"
               placeholder="Password" name="password" required>
        <div class="checkbox mb-3">
        <label>
            <input type="checkbox" value="remember-me"> Remember me
        </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">
            Sign in
        </button>
        <a href="forgetpassword.php">Forgotten password?</a>
        &nbsp;&nbsp;
        <a href="/signup">Sign up</a>

        @include('layouts.errors')

    </form>
@endsection
