@extends('layouts.master')

@section('title', 'moviedekho.com')

@section('css')
    {{ Html::style(mix("css/seat.css")) }}
@endsection

@section('navbar')
<nav class="navbar navbar-inverse" id="page-header">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="/"> Moviedekho.com</a>
            <input type="hidden" id="seat-check-url"
                   value="{{ route('seatCheck') }}">
        </div>
        <div id="show-information">
            <span id="movie">{{ $show->movie_name }}</span>
            <span id="theatre">{{ $show->theatre_name }}-{{ $show->address }}</span>
            <input type="hidden" id="id" value="{{ $id }}">
        </div>
        <div id="proceed-btn">
            <button type="button" class="btn btn-danger"
                    id="proceed">
                Proceed
            </button>
        </div>
    </div>
</nav>
@endsection

@section('content')
<div id="page">
    <div class="container">
        <div class="row col-xs-12">
                @for ($i = 1; $i <= 24; $i++)
                    @if (in_array("A" . $i, $bookedSeatsArray))
                        <span class="seat filled" id="A{{ $i }}">
                            {{ "A" . $i }}
                        </span>
                    @else
                        <span class="seat select pointer" id="A{{ $i }}">
                            {{ "A" . $i }}</span>
                    @endif
                @endfor
        </div>
        @foreach (array("B", "C", "D", "E", "F", "G", "H", "I", "J") as $row)
            <div class="row">
                <div class="col-xs-5 col-xs-offset-1 left">
                    @for ($i = 1; $i <= 10; $i++)
                        @if (in_array($row . $i, $bookedSeatsArray))
                            <span class="seat filled" id="{{ $row . $i }}">
                                {{ $row . $i }}
                            </span>
                        @else
                            <span class="seat select pointer"
                                  id="{{ $row . $i }}">
                                {{ $row . $i }}
                            </span>
                        @endif
                    @endfor
                </div>
                <div class="col-xs-5 right">
                    @for ($i = 11; $i <= 20; $i++)
                        @if (in_array($row . $i, $bookedSeatsArray))
                            <span class="seat filled" id="{{ $row . $i }}">
                                {{ $row . $i }}
                            </span>
                        @else
                            <span class="seat select pointer"
                                  id="{{ $row . $i }}">
                                {{ $row . $i }}
                            </span>
                        @endif
                    @endfor
                </div>
            </div>
        @endforeach
        <div id="bottom">
            <img src="{{ asset('images/Screen.png') }}" alt="Screen">
        </div>
    </div>
</div>
<div>
{{ Form::open([
    'url' => 'https://moviedekho.com/seat/confirmation',
    'method' => 'post',
    'id' => 'form'
    ])
}}
    {{ Form::text('showId', null, ['class' => 'hidden']) }}
    {{ Form::text('seats', null, ['class' => 'hidden']) }}
{{ Form::close() }}
</div>
@endsection

@section('js')
    {{-- CDN reference of Bootbox.js --}}
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js">
    </script>
    {{ Html::script(mix("js/seat.js")) }}
@endsection