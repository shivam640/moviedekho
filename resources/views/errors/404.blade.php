@extends('layouts.master')

@section('title', 'Error')

@section('css')
    {{ Html::style(mix("css/error.css")) }}
@endsection

@section('navbar')
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="/"> Moviedekho.com</a>
            </div>
        </div>
    </nav>
@endsection

@section('content')
    <div class="flex-container">
        <div class="row"><h2>Page Not Found!</h2></div>
    </div>
@endsection
