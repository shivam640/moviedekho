@extends('layouts.master')

@section('title', 'moviedekho.com')

@section('css')
<link rel="stylesheet"
      type="text/css"
      href="https://cdn.datatables.net/v/bs/dt-1.10.16/kt-2.3.2/r-2.2.1/datatables.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
{{ Html::style(mix("css/adminpage.css")) }}
@endsection

@section('navbar')
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle"
                    data-toggle="collapse" data-target="#nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"> Moviedekho.com</a>
        </div>
        <div  class="collapse navbar-collapse" id="nav-collapse">
            <div class="input-group">
                <input type="hidden" id="add-movie-url"
                       value="{{ route('storeMovie') }}">
                <input type="hidden" id="add-theatre-url"
                       value="{{ route('storeTheatre') }}">
                <input type="hidden" id="add-show-url"
                       value="{{ route('storeShow') }}">
                <input type="hidden" id="edit-movie-url"
                       value="{{ route('editMovie') }}">
                <input type="hidden" id="edit-theatre-url"
                       value="{{ route('editTheatre') }}">
                <input type="hidden" id="edit-show-url"
                       value="{{ route('editShow') }}">
            </div>
            <div id="nav_right">
                @if (Auth::check())
                    <ul class="nav navbar-nav">
                        <li><span id="username">
                        {{ "Hi, " . Auth::user()->name }}
                        </span></li>
                        <li>
                            <a href="/logout">
                                <span class="glyphicon glyphicon-log-out">
                                </span> Log Out</a>
                        </li>
                    </ul>
                @endif
            </div>
        </div>
    </div>
</nav>
@endsection

@section('content')
<div id="page">
	<div class="container">
        <ul class="nav nav-tabs">
            <li class="active">
                <a data-toggle="tab" href="#movies">
                    Movies
                </a>
            </li>
            <li>
                <a data-toggle="tab" href="#theatres">
                    Theatres
                </a>
            </li>
            <li>
                <a data-toggle="tab" href="#shows">
                    shows
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div id="movies" class="tab-pane fade in active">
                <div class="row table-responsive">
                    <table id="movie-table" class="table table-bordered table-hover" style="width:100%">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Duration</th>
                            <th>Director</th>
                            <th>Casts</th>
                            <th>Description</th>
                            <th>Banner</th>
                            <th>Edit / Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <a id="adding-movie" class="btn btn-primary pull-right
                    addProjectModal">
                        Add Movie</a>
                    <!-- Modal -->
                    <div class="modal fade" id="store-movie" tabindex="-1"
                         role="dialog">
                    </div>
                </div>
            </div>
            <div id="theatres" class="tab-pane fade">
                <div class="row table-responsive">
                    <table id="theatre-table" class="table table-bordered table-hover" style="width:100%">
                        <thead>
                        <tr>
                            <th>Theatre Name</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Screen(Capacity)</th>
                            <th>Edit / Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <a id="adding-theatre" class="btn btn-primary pull-right">
                        Add Theatre</a>
                    <!-- Modal -->
                    <div class="modal fade" id="store-theatre" tabindex="-1"
                         role="dialog">
                    </div>
                </div>
            </div>
            <div id="shows" class="tab-pane fade">
                <div class="row table-responsive">
                    <table id="show-table"
                           class="table table-bordered table-hover"
                           style="width:100%">
                        <thead>
                        <tr>
                            <th>Theatre</th>
                            <th>Screen</th>
                            <th>date</th>
                            <th>Show time</th>
                            <th>Movie</th>
                            <th>Edit / Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <a id="adding-show" class="btn btn-primary pull-right
                    addProjectModal">
                        Add Show</a>
                    <!-- Modal -->
                    <div class="modal fade" id="store-show" tabindex="-1"
                         role="dialog">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
{{-- CDN reference of Bootbox.js --}}
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js">
</script>
{{-- CDN reference of daterangepicker.js --}}
<script type="text/javascript"
        src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js">
</script>
{{-- CDN reference of datatables.js --}}
<script type="text/javascript"
        src="https://cdn.datatables.net/v/bs/dt-1.10.16/kt-2.3.2/r-2.2.1/datatables.min.js">
</script>
{{-- CDN reference of jquery validate plugin --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/additional-methods.min.js">
</script>

{{ Html::script(mix("js/adminpage.js")) }}
@endsection