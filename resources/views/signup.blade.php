@extends('layouts.master')

@section('title', 'moviedekho.com')

@section('css')
{{ Html::style(mix("css/login.css")) }}
@endsection

@section('content')
<div class="text-center">
    <form class="form-signin" method="POST" action="/signup">
        @csrf
        <h1 class="h3 mb-3 font-weight-normal">Please register</h1>
        <hr>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" id="inputEmail" class="form-control" name="email"
               placeholder="Email address" autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" name="password"
             class="form-control" placeholder="Password">
        <label for="name" class="sr-only">Name</label>
        <input type="text" id="name" name="name"  class="form-control"
             placeholder="Name">
        <label for="phone" class="sr-only">Phone</label>
        <input type="text" id="phone" name="phone" class="form-control"
             placeholder="Phone">
        <br>
        <button class="btn btn-lg btn-primary btn-block" type="submit">
          Sign up
        </button>
@include('layouts.errors')
    </form>
@endsection
