@extends('layouts.master')

@section('title', 'moviedekho.com')

@section('css')
    {{ Html::style(mix("css/payment.css")) }}
@endsection

@section('navbar')
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="/"> Moviedekho.com</a>
                <input type="hidden" id="get-session-url"
                       value="{{ route('getSession') }}">
                <input type="hidden" id="seat-check-url"
                       value="{{ route('seatCheck') }}">
            </div>
            <div id="need-help">
                <p><i id="phone-icon" class="fa fa-phone"></i>
                    &nbsp;
                        {{ Config::get('constants.PHONE_1') }}
                        , {{ Config::get('constants.PHONE_2') }}
                </p>
            </div>
        </div>
    </nav>
@endsection

@section('content')
<div id="page">
    <div class="container">
        <div class="col-md-8">
            @if($message = Session::get('errorMessage'))
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">
                        x
                    </button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            {{ Form::open(['route' => 'payment', 'id' => 'card-info']) }}
                <div class="col-md-12" id="left-div">
                    <div>
                        <div id="email-phone-header">
                            Share your Contact Details
                        </div>
                        <div class="row" id="email-phone-info">
                            <div class="form-group col-md-5">
                                {{ Form::text(
                                    'email',
                                    $user->email, 
                                    ['class' => 'form-control',
                                     'placeholder' => 'Email Address']
                                ) }}
                            </div>
                            <div class="form-group col-md-5">
                                {{ Form::text(
                                    'phone', 
                                    $user->phone, 
                                    ['class' => 'form-control',
                                    'placeholder' => 'Phone Number']
                                ) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" id="left-div">
                    <div>
                        <div id="card-header">
                            Enter your Card Details
                        </div>
                        <div class="form-group col-md-6">
                            <div id="card-form">
                                <div class="row">
                                    <div class="field-header">Card Number</div>
                                    <div class="form-group col-md-8">
                                    {{ Form::text(
                                        'cardNumber',
                                        null, 
                                        ['class' => 'form-control',
                                        'placeholder' => 'Enter card number']
                                    ) }}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-8">
                                    {{ Form::text(
                                        'cardName',
                                        null, 
                                        ['class' => 'form-control', 
                                        'placeholder' => 'Name on the Card']
                                    ) }}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="field-header">Expiry</div>
                                        <div class="form-group col-md-6">
                                            {{ Form::text(
                                                'expiryMonth',
                                                null, 
                                                ['class' => 'form-control', 
                                                'placeholder' => 'MM']
                                            ) }}
                                        </div>
                                        <div class="form-group col-md-6">
                                            {{ Form::text(
                                                'expiryYear',
                                                null, 
                                                ['class' => 'form-control', 
                                                'placeholder' => 'YY']
                                            ) }}
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="field-header">CVV</div>
                                        <div class="form-group col-md-9">
                                            {{ Form::password(
                                                'cvvNumber',
                                                ['class' => 'form-control', 
                                                'placeholder' => 'CVV']
                                            ) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row" id="error-div"></div>
                            <div class="row" id="make-payment">
                                {{ Form::button(
                                    'Make Payment',
                                    ['class' => 'btn btn-danger',
                                        'id' => 'payment-submit']
                                ) }}
                            </div>
                        </div>
                    </div>
                </div>
            {{ Form::close() }}
        </div>
        @php
            $count = count($seats);
            $total = $count * Config::get('constants.COST');
            $bookedSeatsString = implode(" ", $seats);
        @endphp
        <div class="col-md-4" id="right-div">
            <input type="hidden" id="show-id"
                   value="{{ $showId }}">
            <input type="hidden" id="booked-seats"
                   value="{{ $bookedSeatsString }}">
            <p id="order-header">ORDER SUMMARY</p>
            <h2>{{ $show->movie_name }}</h2>
            <p id="theatre">
                {{ $show->theatre_name }}:{{ $show->address }}
                (SCREEN {{ $show->screen_name }})
            </p>
            <p>
                @foreach( $seats as $seat )
                @if ($loop->last)
                    {{ $seat }}
                    @break
                @endif
                    {{ $seat }},
                @endforeach
                &nbsp;
                <strong>({{ $count }} ticket)</strong>
                <br>
                {{ date("h:i a", strtotime($show->time)) }},
                {{ date("d M Y", strtotime($show->date)) }}
            </p>
            <hr>
            <p>
                Sub Total:
                <span id="sub-total">Rs. {{ $total }}</span>
            </p>
            <p>
                +internet handling fees
                <span id="internet-fees">
                    Rs. {{ Config::get('constants.INTERNET_FEES') }}
                </span>
            </p>
            <p>
                <strong>
                    Amount payable 
                    <span id="amount">
                        Rs.{{ $amount }}
                    </span>
                </strong>
            </p>
        </div>
        <div id="timeout">
            <p>Your session will expire in <span id="time"></span></p>
        </div>
        @if ($errors->any())
            <div class="col-md-4">
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">
                        x
                    </button>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
    </div>
</div>
@endsection

@section('js')
{{-- CDN reference of Bootbox.js --}}
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js">
</script>
{{-- CDN reference of cookies.js --}}
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js">
</script>
{{-- CDN reference of jquery validate plugin --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/additional-methods.min.js">
</script>
{{ Html::script(mix("js/payment.js")) }}
@endsection