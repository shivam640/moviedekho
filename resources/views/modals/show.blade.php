{{ Form::open(['id' =>  'add-show-form']) }}
<div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <label class="modal-title"
                id="exampleModalLongTitle">
            </label>
            <button type="button" class="close"
                    data-dismiss="modal"
                    aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div id="show-error">
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    {{ Form::label(
                        'theatre',
                        'Theatre:'
                    ) }}
                    {{ Form::select('theatre',  $theatreArray,
                        isset($theatre) ? $theatre->theatre_id : null, [
                        'placeholder' => '',
                        'class' => 'form-control',
                        'id' => 'theatre-selected']
                    ) }}
                </div>
                <div class="form-group col-md-6">
                    {{ Form::label(
                        'screen',
                        'Screen:'
                    ) }}
                    {{ Form::select('screen', $screenArray,
                        isset($show) ? $show->screen_id : null, [
                        'placeholder' => '',
                        'class' => 'form-control',
                        'id' => 'select-screen']
                    ) }}
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    {{ Form::label(
                        'movie',
                        'Movie:'
                    ) }}
                    {{ Form::select('movie', $movieArray,
                        isset($show) ? $show->movie_id : null, [
                        'placeholder' => '',
                        'class' => 'form-control',
                        'id' => 'movie-id']
                    ) }}
                </div>
                <div class="form-group col-md-6">
                    {{ Form::label(
                        'time',
                        'Show time:'
                    ) }}
                    {{ Form::select('time', [
                        '11:00:00' => '11:00 AM',
                        '14:00:00' => '02:00 PM',
                        '17:00:00' => '05:00 PM',
                        '20:00:00' => '08:00 PM',
                        '23:00:00' => '11:00 PM'],
                        isset($show) ? $show->time : null, [
                        'placeholder' => '',
                        'class' => 'form-control']
                    ) }}
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    {{ Form::label(
                        'date',
                        'Dates:'
                    ) }}
                    {{ Form::text(
                        'date',
                        isset($show) ? date('m/d/Y', strtotime($show->date))
                         : null, [
                        'class' => 'form-control',
                        'value' => '']
                    ) }}
                </div>
            </div>
            {{ Form::submit('Submit', ['class' => 'btn btn-primary']) }}
        </div>
    </div>
</div>
{{ Form::close() }}
