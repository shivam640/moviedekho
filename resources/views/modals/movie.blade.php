{{ Form::open(['id' => 'add-movie-form', 'files' => true]) }}
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label class="modal-title"
                       id="exampleModalLongTitle">
                </label>
                <button type="button" class="close"
                        data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="movie-error"></div>
                <div class="row">
                    <div class="form-group col-md-6">
                        {{ Form::label(
                            'movieName',
                             'Name:'
                        ) }}
                        {{ Form::text(
                            'movieName',
                            isset($movie) ? $movie->movie_name : null,
                            ['class' => 'form-control']
                        ) }}
                    </div>
                    <div class="form-group col-md-6">
                        {{ Form::label(
                            'duration',
                            'Duration(in mins):'
                        ) }}
                        {{ Form::number(
                            'duration',
                            isset($movie) ? $movie->duration : null,
                            ['class' => 'form-control']
                        ) }}
                    </div>
                </div>
                <div class="row">
                    <div class="form-group  col-md-6">
                        {{ Form::label(
                            'director',
                            'Director:'
                        ) }}
                        {{ Form::text(
                            'director',
                            isset($movie) ? $movie->director : null,
                            ['class' => 'form-control']
                        ) }}
                    </div>
                    <div class="form-group  col-md-6">
                        {{ Form::label(
                            'casts',
                            'Casts:'
                        ) }}
                        {{ Form::text(
                            'casts',
                            isset($movie) ? $movie->cast : null,
                            ['class' => 'form-control']
                        ) }}
                    </div>
                </div>
                <div class="row">
                    <div class="form-group  col-md-12">
                        {{ Form::label(
                            'description',
                            'Description:'
                        ) }}
                        {{ Form::textarea(
                            'description',
                            isset($movie) ? $movie->description : null,
                            ['class' => 'form-control', 'rows' => '4']
                        ) }}
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        {{ Form::label(
                            'poster',
                            'Poster:'
                        ) }}
                        {{ Form::file('poster', ['class' => 'form-control']) }}
                    </div>
                    @if (isset($movie))
                        <div class="form-group col-md-6">
                            <input type="hidden" id="hidden-poster"
                                   value="{{ $movie->poster }}">
                            <img src="/storage/app/{{ $movie->poster }}" width="200px">
                        </div>
                    @endif
                </div>
                {{ Form::submit('Submit', ['class' => 'btn btn-primary']) }}
            </div>
        </div>
    </div>
{{ Form::close() }}