{{ Form::open(['id' => 'add-theatre-form']) }}
<div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <label class="modal-title">
            </label>
            <button type="button" class="close"
                    data-dismiss="modal"
                    aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div id="theatre-error"></div>
            <div class="row">
                <div class="form-group col-md-12">
                    {{ Form::label('theatreName', 'Name:') }}
                    {{ Form::text(
                        'theatreName',
                        isset($theatre) ? $theatre->theatre_name : null,
                        ['class' => 'form-control']
                    ) }}
                </div>
            </div>
            <div class="row">
                <div class="form-group  col-md-12">
                    {{ Form::label('address', 'Address:') }}
                    {{ Form::text(
                        'address',
                        isset($theatre) ? $theatre->address : null,
                        ['class' => 'form-control']
                    ) }}
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    {{ Form::label(
                        'city',
                        'City:'
                    ) }}
                    {{ Form::text(
                        'city',
                        isset($theatre) ? $theatre->city : null,
                        ['class' => 'form-control']
                    ) }}
                </div>
                <div class="form-group col-md-6">
                    {{ Form::label(
                        'state',
                        'State:'
                    ) }}
                    {{ Form::select('state', [
                        'Odisha' => 'Odisha',
                        'West Bengl' => 'West Bengl',
                        'Karnataka' => 'Karnataka',
                        'Chattisgarh' => 'Chattisgarh',
                        'Jharkhand' => 'Jharkhand',
                        'Bihar' => 'Bihar'],
                        isset($theatre) ? $theatre->state : null, [
                        'placeholder' => '',
                        'class' => 'form-control']
                    ) }}
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    {{ Form::label(
                        'screenName',
                        'Screens:'
                    ) }}
                </div>
            </div>
                @if (isset($screens))
                    @foreach ($screens as $key=>$screen)
                        <div class="row">
                            <div class="form-group col-md-4 col-md-offset-1">
                                {{ Form::text(
                                    'screens[' . $screen->id . ']',
                                    $screen->screen_name, [
                                    'class' => 'form-control',
                                    'placeholder' => 'Name']
                                ) }}
                            </div>
                            <div class="form-group col-md-4">
                                {{ Form::text(
                                    'capacity[' . $screen->id . ']',
                                    $screen->capacity, [
                                    'class' => 'form-control',
                                    'placeholder' => 'Capacity']
                                ) }}
                            </div>
                            @if ($loop->first)
                                <div class="form-group col-md-1">
                                    <button type="button"
                                            class="btn btn-default add-button">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            @else
                                <div class="form-group col-md-1">
                                    <button type="button"
                                            class="btn btn-default remove-button">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            @endif
                        </div>
                    @endforeach
                @else
                    <div class="row">
                        <div class="form-group col-md-4 col-md-offset-1">
                            {{ Form::text(
                                'screens[0]',
                                '', [
                                'class' => 'form-control',
                                'placeholder' => 'Name',
                                'id' => 'screen-0']
                            ) }}
                        </div>
                        <div class="form-group col-md-4">
                            {{ Form::text(
                                'capacity[0]',
                                '', [
                                'class' => 'form-control',
                                'placeholder' => 'Capacity',
                                'id' => 'capacity-0'
                                ]
                            ) }}
                        </div>
                        <div class="form-group col-md-1">
                            <button type="button"
                                    class="btn btn-default add-button">
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="form-group input-fields-container"></div>
                </div>
            {{ Form::submit('Submit', ['class' => 'btn btn-primary']) }}
        </div>
    </div>
</div>
{{ Form::close() }}