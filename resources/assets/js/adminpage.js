var showDataTable;
var movieDataTable;
var theatreDataTable;

var movieTable = function() {
    var movieColumns = [
        {"data": 'movie_name'},
        {"data": 'duration'},
        {"data": 'director'},
        {"data": 'cast'},
        {"data": 'description'}
    ];

    movieColumns.push({
        mData: "Banner",
        bSortable: false,
        mRender: function (data, type, row) {
            var image = '<img src="/storage/app/' + row.poster + '" width="200px">';
            return image;
        }
    });

    movieColumns.push({
        mData: "Action",
        bSortable: false,
        mRender: function () {
            var button = '<a href="javascript:void(0)" class="editing-movie">' +
                '<i class="fa fa-edit"></i></a> ' +
                '<a href="javascript:void(0)" class="movie-delete">' +
                '<i class="fa fa-trash"></i></a>';
            return button;
        }
    });

    movieDataTable = $('#movie-table').DataTable({
        "pageLength": 5,
        "processing": true,
        "serverSide": true,
        "ajax": '/admin/movie-data-table',
        "columns": movieColumns,
    });
};

var theatreTable = function() {
    var theatreColumns = [
        {"data": 'theatre_name'},
        {"data": 'address'},
        {"data": 'city'},
        {"data": 'state'},
        {"data": 'screens'}
    ];

    theatreColumns.push({
        mData: "Action",
        bSortable: false,
        mRender: function () {
            var button = '<a href="javascript:void(0)" class="editing-theatre">' +
                'Edit </a>/ ' +
                '<a  class="theatre-delete">' +
                'Delete</a>';
            return button;
        }
    });

    theatreDataTable = $('#theatre-table').DataTable({
        "pageLength": 10,
        "processing": true,
        "serverSide": true,
        "ajax": '/admin/theatre-data-table',
        "columns": theatreColumns,
    });
};


var showTable = function() {
    var showColumns = [
        {"data": 'theatre_name'},
        {"data": 'screen_name'},
        {"data": 'date'},
        {"data": 'time'},
        {"data": 'movie_name'}
    ];

    showColumns.push({
        mData: "Action",
        bSortable: false,
        mRender: function () {
            var button = '<a href="javascript:void(0)" class="editing-show">' +
                'Edit </a>/ ' +
                '<a  class="show-delete">' +
                'Delete</a>';
            return button;
        }
    });

    showDataTable = $('#show-table').DataTable({
        "pageLength": 10,
        "processing": true,
        "serverSide": true,
        "ajax": '/admin/show-data-table',
        "columns": showColumns,
    });
};

$(document).ready(function() {

    showTable();
    movieTable();
    theatreTable();

    var maxFieldsLimit = 5;
    var counter = 1;
    var showId;
    var movieId;
    var theatreId;

    $(document).on('click','.add-button', function(e) {
        e.preventDefault();
        if(counter < maxFieldsLimit) {
            // adding input field
            $(".input-fields-container").append(
                '<div><div class="form-group col-md-4 col-md-offset-1">' +
                '<input data-rule-required="true" type="text"' +
                ' class="form-control" placeholder="Name"' +
                ' name="screens[' + counter + ']" ' +
                'id="screen-' + counter + '"></div>' +
                '<div class="form-group col-md-4">' +
                '<input data-rule-required="true" type="number"' +
                ' class="form-control" id="capacity-' + counter + '"' +
                ' placeholder="Capacity" name="capacity[' + counter + ']">' +
                '</div>' +
                '<div class="form-group col-md-1">' +
                '<button type="button" class="btn btn-default remove-button"' +
                ' id="remove-' + counter + '">' +
                '<i class="fa fa-minus"></i>' +
                '</button></div></div>');
            counter++;
        } else {
            bootbox.alert({
                title: "Alert",
                message: "<strong>Maximum 5 screens can be added!</strong>"
            })
        }
    });

    $(document).on('click', '.remove-button', function(e) {
        e.preventDefault();
        $(this).parent("div").parent("div").remove();
        counter--;
    });

    function validateMovie(movie) {
        movie.validate({
            rules: {
                movieName: "required",
                poster: {
                    required: function () {
                        var required = false;
                        if ('add-movie-form' === movie.attr('id')) {
                            required = true;
                        } else if ($('#hidden-poster').val() === '') {
                            required = true;
                        }
                        return required;
                    }
                },
                duration: {
                    required: true,
                    number: true,
                    min: 1,
                    max: 200
                }
            },
            messages: {
                movieName: "Movie name is required",
                poster: "Please select an image",
                duration: {
                    required: "Duration is required",
                    number: "Enter valid number",
                    min: "Cannot be zero",
                    max: "No more than 200"
                }
            }, highlight: function (element) {
                $(element).addClass("error");
            }, unhighlight: function (element) {
                $(element).removeClass("error");
            },
            errorPlacement: function (label, element) {
                label.addClass("box");
                label.insertBefore(element);
            }
        });
    }

    function validateTheatre(theatre) {
        theatre.validate({
            rules: {
                theatreName: "required",
                address: "required",
                city: "required",
                state: "required",
                "screens[0]": "required",
                "capacity[0]": "required"
            },
            messages: {
                theatreName: "Movie name is required",
                address: "address is required",
                city: "city is required",
                state: "state is required",
            }, highlight: function (element) {
                $(element).addClass("error");
            }, unhighlight: function (element) {
                $(element).removeClass("error");
            },
            errorPlacement: function (label, element) {
                label.addClass("box");
                label.insertBefore(element);
            }
        });
    }

    $('#adding-movie').click( function () {
        var modal = $('#store-movie');
        modal.load('/adminpage/movie/modal/', function () {
            modal.modal('show');
        });
        modal.on('shown.bs.modal', function() {
            $("form#edit-movie-form").prop('id','add-movie-form');
            $('#store-movie .modal-title').html("Add Movie");
        });
    });

    $(document).on('submit','#add-movie-form', function(e){
        var modal = $('#store-movie');
        e.preventDefault();
        validateMovie($(this));
        if ($(this).valid()) {
            var text = "";
            var formData = new FormData(this);

            $.ajax({
                type: "POST",
                url: $('#add-movie-url').val(),
                data: formData,
                processData: false,
                contentType: false,
                success: function (msg) {
                    if (true === msg.result) {
                        movieDataTable.ajax.reload();
                        modal.modal('hide');
                        $('.container').prepend("<Span id='message'" +
                            " class='alert alert-success alert-block'" +
                            " role='alert'>" +
                            "<strong>Movie added Successfully</strong>" +
                            "</Span>");
                        $("#message").fadeOut(7000);
                    } else {
                        $("#movie-error").addClass("alert alert-danger");
                        $("#movie-error")
                            .html("<p>" + "Unable to process your request," +
                                " Try again later!" + "</p>");
                    }
                },
                error: function (error) {
                    var errors = error.responseJSON.errors;
                    for (var errorName in errors) {
                        text += "# " + errors[errorName] + "<br>";
                    }
                    $("#movie-error").addClass("alert alert-danger");
                    $("#movie-error").html("<p>" + text + "</p>");
                }
            });
        }
    });

    $(document).on( 'click', '.editing-movie', function() {
        movieId = $(this).closest('tr').attr('id');
        var modal = $('#store-movie');
        modal.load('/adminpage/movie/modal/' + movieId, function () {
            modal.modal('show');
        });
        modal.on('shown.bs.modal', function() {
            $("form#add-movie-form").prop('id','edit-movie-form');
            $('#store-movie .modal-title').html("Edit Movie");
        });
    });

    $(document).on('submit','#edit-movie-form', function(e){
        var modal = $('#store-movie');
        e.preventDefault();
        validateMovie($('#edit-movie-form'));
        if ($(this).valid()) {
            var text = "";
            var formData = new FormData(this);
            formData.append('movieId', movieId);

            $.ajax({
                type: "POST",
                url: $('#edit-movie-url').val(),
                data: formData,
                processData: false,
                contentType: false,
                success: function (msg) {
                    if (true === msg.result) {
                        movieDataTable.ajax.reload();
                        modal.modal('hide');
                        $('.container').prepend("<Span id='message'" +
                            " class='alert alert-success alert-block'" +
                            " role='alert'>" +
                            "<strong>Movie updated Successfully</strong>" +
                            "</Span>");
                        $("#message").fadeOut(7000);
                    } else {
                        $("#movie-error").addClass("alert alert-danger");
                        $("#movie-error")
                            .html("<p>" + "Unable to process your request," +
                                " Try again later!" + "</p>");
                    }
                },
                error: function (error) {
                    var errors = error.responseJSON.errors;
                    for (var errorName in errors) {
                        text += "# " + errors[errorName] + "<br>";
                    }
                    $("#movie-error").addClass("alert alert-danger");
                    $("#movie-error").html("<p>" + text + "</p>");
                }
            });
        }
    });

    $('#store-movie').on('shown.bs.modal', function() {
        $('input[name=movieName]').focus();
    });

    $(document).on('change','#theatre-selected', function(){
        var id = $("#theatre-selected option:selected").val();
        $.ajax({
            type: "POST",
            url: '/load/screen',
            data: {theatreId: id },
            success: function(response){
                if (false === response) {
                    $("#theatre-error").addClass("alert alert-danger");
                    $("#theatre-error")
                        .html("<p>" + "Unable to process your request," +
                            " Try again later!" + "</p>");
                } else {
                    var select = $('#select-screen');
                    select.empty();
                    $.each(response, function (index, object) {
                        select.append('<option value="' + object.id + '">'
                            + object.screen_name + '</option>');
                    });
                }
            }
        });
    });

    $('#adding-theatre').click( function () {
        var modal = $('#store-theatre');
        modal.load('/adminpage/theatre/modal/', function () {
            modal.modal('show');
            $("form#edit-theatre-form").prop('id','add-theatre-form');
            $('#store-theatre .modal-title').html("Add Theatre");
        });
        modal.on('shown.bs.modal', function() {
            counter = 1;
        });
    });

    $(document).on('submit','#add-theatre-form', function(e){
        var modal = $('#store-theatre');
        e.preventDefault();
        validateTheatre($(this));
        if ($(this).valid()) {
            var text = "";
            $.ajax({
                type: "POST",
                url: $('#add-theatre-url').val(),
                data: $(this).serialize(),
                success: function (msg) {
                    if (true === msg.result) {
                        theatreDataTable.ajax.reload();
                        modal.modal('hide');
                        $('.container').prepend("<Span id='message'" +
                            " class='alert alert-success alert-block'" +
                            " role='alert'>" +
                            "<strong>Theatre added Successfully</strong>" +
                            "</Span>");
                        $("#message").fadeOut(7000);
                    } else {
                        $("#theatre-error").addClass("alert alert-danger");
                        $("#theatre-error")
                            .html("<p>" + "Unable to process your request," +
                                " Try again later!" + "</p>");
                    }
                },
                error: function (error) {
                    console.log('error');
                    var errors = error.responseJSON.errors;
                    for (var errorName in errors) {
                        text += "# " + errors[errorName] + "<br>";
                    }
                    $("#theatre-error").addClass("alert alert-danger");
                    $("#theatre-error").html("<p>" + text + "</p>");
                }
            });
        }
    });

    $(document).on( 'click', '.editing-theatre', function() {
        theatreId = $(this).closest('tr').attr('id');
        var modal = $('#store-theatre');
        modal.load('/adminpage/theatre/modal/' + theatreId, function () {
            modal.modal('show');
            $("form#add-theatre-form").prop('id','edit-theatre-form');
            $('#store-theatre .modal-title').html("Edit Theatre");
        });
    });

    $(document).on('submit','#edit-theatre-form', function(e){
        var modal = $('#store-theatre');
        e.preventDefault();
        validateTheatre($(this));
        if ($(this).valid()) {
            var text = "";

            $.ajax({
                type: "POST",
                url: $('#edit-theatre-url').val(),
                data: $(this).serialize() + "&theatreId=" + theatreId + "",
                success: function (msg) {
                    if (true === msg.result) {
                        theatreDataTable.ajax.reload();
                        modal.modal('hide');
                        $('.container').prepend("<Span id='message'" +
                            " class='alert alert-success alert-block'" +
                            " role='alert'>" +
                            "<strong>Theatre updated Successfully</strong>" +
                            "</Span>");
                        $("#message").fadeOut(7000);
                    } else {
                        $("#theatre-error").addClass("alert alert-danger");
                        $("#theatre-error")
                            .html("<p>" + "Unable to process your request," +
                                " Try again later!" + "</p>");
                    }
                },
                error: function (error) {
                    var errors = error.responseJSON.errors;
                    for (var errorName in errors) {
                        text += "# " + errors[errorName] + "<br>";
                    }
                    $("#theatre-error").addClass("alert alert-danger");
                    $("#theatre-error").html("<p>" + text + "</p>");
                }
            });
        }
    });

    function validateShow(show) {
        show.validate({
            rules: {
                theatre: 'required',
                screen: 'required',
                movie: 'required',
                time: 'required',
                date: 'required'
            }, highlight: function (element) {
                $(element).addClass('error');
            }, unhighlight: function (element) {
                $(element).removeClass('error');
            },
            errorPlacement: function (label, element) {
                label.addClass('box');
                label.insertBefore(element);
            }
        });
    }
    // $(document).on('click', '#store-show #date', function() {
    //     $("#store-show").css("overflow", "auto");
    // });

    $('#adding-show').on( 'click', function() {
        var modal = $('#store-show');
        if ($("#date").length) {
            $('#date').data('daterangepicker').remove() ;
        }
        modal.load('/adminpage/show/modal/', function () {
            modal.modal('show');
            $("form#edit-show-form").prop('id','add-show-form');
            $('#store-show .modal-title').html("Add Show");
            $('input[name="date"]').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear'
                }
            });
            $('input[name="date"]')
                .on('apply.daterangepicker', function (ev, picker) {
                    $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' +
                        picker.endDate.format('MM/DD/YYYY'));
                });
            $('input[name="date"]')
                .on('cancel.daterangepicker', function (ev, picker) {
                    $(this).val('');
                });

        });
    });

    $(document).on('submit','#add-show-form', function(e){
        var modal = $('#store-show');
        e.preventDefault();
        validateShow($(this));
        if ($(this).valid()) {
            var text = "";

            $.ajax({
                type: "POST",
                url: $('#add-show-url').val(),
                data: $(this).serialize(),
                success: function (msg) {
                    if (true === msg.result) {
                        showDataTable.ajax.reload();
                        modal.modal('hide');
                        $('.container').prepend("<Span id='message'" +
                            " class='alert alert-success alert-block'" +
                            " role='alert'>" +
                            "<strong>Show added Successfully</strong>" +
                            "</Span>");
                        $("#message").fadeOut(7000);
                    } else {
                        $("#show-error").addClass("alert alert-danger");
                        $("#show-error")
                            .html("<p>" + "Unable to process your request," +
                                " Try again later!" + "</p>");
                    }
                },
                error: function (error) {
                    var errors = error.responseJSON.errors;
                    for (var errorName in errors) {
                        text += "# " + errors[errorName] + "<br>";
                    }
                    $("#show-error").addClass("alert alert-danger");
                    $("#show-error").html("<p>" + text + "</p>");
                }
            });
        }
    });

    $(document).on( 'click', '.editing-show', function() {
        showId = $(this).closest('tr').attr('id');
        var modal = $('#store-show');
        modal.load('/adminpage/show/modal/' + showId, function () {
            modal.modal('show');
            $("form#add-show-form").prop('id','edit-show-form');
            $('#store-show .modal-title').html("Edit Show");
            $('input[name="date"]').daterangepicker({
                singleDatePicker: true,
                autoUpdateInput: false,
                showDropdowns: true
            });
        });
    });

    $(document).on('submit','#edit-show-form', function(e){
        var modal = $('#store-show');
        e.preventDefault();
        validateShow($(this));
        if ($(this).valid()) {
            var text = "";

            $.ajax({
                type: "POST",
                url: $('#edit-show-url').val(),
                data: $(this).serialize() + "&id=" + showId + "",
                success: function (msg) {
                    if (true === msg.result) {
                        showDataTable.ajax.reload();
                        modal.modal('hide');
                        $('.container').prepend("<Span id='message'" +
                            " class='alert alert-success alert-block'" +
                            " role='alert'>" +
                            "<strong>Show updated Successfully</strong>" +
                            "</Span>");
                        $("#message").fadeOut(7000);
                    } else {
                        $("#show-error").addClass("alert alert-danger");
                        $("#show-error")
                            .html("<p>" + "Unable to process your request," +
                                " Try again later!" + "</p>");
                    }
                },
                error: function (error) {
                    var errors = error.responseJSON.errors;
                    for (var errorName in errors) {
                        text += "# " + errors[errorName] + "<br>";
                    }
                    $("#show-error").addClass("alert alert-danger");
                    $("#show-error").html("<p>" + text + "</p>");
                }
            });
        }
    });

    $(document).on('click','.movie-delete', function(){
        var grandParent = $(this).parent().parent();
        var id = grandParent.attr('id');
        bootbox.confirm({
            title: "Confirmation",
            message: "Do you want to delete this movie? This cannot be undone.",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function (result) {
                if (result) {
                    $.ajax({
                        type: "POST",
                        url: '/movie/delete',
                        data: {id: id},
                        success: function (msg) {
                            if (true === msg.result) {
                                movieDataTable.ajax.reload();
                                $('.container').prepend("<Span id='message'" +
                                    " class='alert alert-success alert-block'" +
                                    " role='alert'>" +
                                    "<strong>Movie deleted Successfully</strong>" +
                                    "</Span>");
                                $("#message").fadeOut(7000);
                            } else {
                                $('.container').prepend("<Span id='message'" +
                                    " class='alert alert-danger alert-block'" +
                                    " role='alert'>" +
                                    "<strong>Unable to delete movie,  Try again later!</strong>" +
                                    "</Span>");
                                $("#message").fadeOut(7000);
                            }
                        },
                        error: function (error) {
                            $('.container').prepend("<Span id='message'" +
                                " class='alert alert-danger alert-block'" +
                                " role='alert'>" +
                                "<strong>Unable to process your request,  Try again later!</strong>" +
                                "</Span>");
                            $("#message").fadeOut(7000);
                        }
                    });
                }
            }
        });
    });

    $(document).on('click','.theatre-delete', function() {
        var grandParent = $(this).parent().parent();
        var id = grandParent.attr('id');
        bootbox.confirm({
            title: "Confirmation",
            message: "Do you want to delete this theatre? This cannot be undone.",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function (result) {
                if (result) {
                    $.ajax({
                        type: "POST",
                        url: '/theatre/delete',
                        data: {id: id},
                        success: function (msg) {
                            if (true == msg.result) {
                                theatreDataTable.ajax.reload();
                                $('.container').prepend("<Span id='message'" +
                                    " class='alert alert-success alert-block'" +
                                    " role='alert'>" +
                                    "<strong>Theatre deleted Successfully</strong>" +
                                    "</Span>");
                                $("#message").fadeOut(7000);
                            } else {
                                $('.container').prepend("<Span id='message'" +
                                    " class='alert alert-danger alert-block'" +
                                    " role='alert'>" +
                                    "<strong>Unable to delete theatre,  Try again later!</strong>" +
                                    "</Span>");
                                $("#message").fadeOut(7000);
                            }
                        },
                        error: function (error) {
                            $('.container').prepend("<Span id='message'" +
                                " class='alert alert-danger alert-block'" +
                                " role='alert'>" +
                                "<strong>Unable to process your request,  Try again later!</strong>" +
                                "</Span>");
                            $("#message").fadeOut(7000);
                        }
                    });
                }
            }
        });
    });

    $(document).on('click','.show-delete', function(){
        var grandParent = $(this).parent().parent();
        var id = grandParent.attr('id');
        bootbox.confirm({
            title: "Confirmation",
            message: "Do you want to delete this show? This cannot be undone.",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function (result) {
                if (result) {
                    // window.location = '/show/delete/' + id;
                    $.ajax({
                        type: "POST",
                        url: '/show/delete',
                        data: {id: id},
                        success: function (msg) {
                            if (true == msg.result) {
                                showDataTable.ajax.reload();
                                $('.container').prepend("<Span id='message'" +
                                    " class='alert alert-success alert-block'" +
                                    " role='alert'>" +
                                    "<strong>Show deleted Successfully</strong>" +
                                    "</Span>");
                                $("#message").fadeOut(7000);
                            } else {
                                $('.container').prepend("<Span id='message'" +
                                    " class='alert alert-danger alert-block'" +
                                    " role='alert'>" +
                                    "<strong>Unable to delete show,  Try again later!</strong>" +
                                    "</Span>");
                                $("#message").fadeOut(7000);
                            }
                        },
                        error: function (error) {
                            $('.container').prepend("<Span id='message'" +
                                " class='alert alert-danger alert-block'" +
                                " role='alert'>" +
                                "<strong>Unable to process your request,  Try again later!</strong>" +
                                "</Span>");
                            $("#message").fadeOut(7000);
                        }
                    });
                }
            }
        });
    });

});


