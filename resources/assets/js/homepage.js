var movieDataTable;

function formatDate() {
    var d = new Date(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

var movieTable = function() {
    var showColumns = [
        {
            mData: "Card",
            bSortable: false,
            mRender: function (data, type, row) {
                var image = '<div id="section">' +
                            '<img id="banner" src="storage/app/' + row.poster + '" width=100% height="140">' +
                            '<div id="information">' +
                            '<a href="movie/details/' + row.DT_RowId + '/formatSate()">' +
                            '<h4>' + row.movie_name + '</h4></a>' +
                            '<p><b>Duration:</b>' + row.duration + ' mins<br>' +
                            '<b>Director:</b> ' + row.director + '' +
                            '<span id="cast">' +
                            '<b>Casts:</b> ' + row.cast + '' +
                            '</span>' +
                            '<a class="btn btn-secondary"' +
                            'href="movie/details/' + row.DT_RowId + '/formatSate()" role="button">' +
                            'View details</a></p></div></div>';
                return image;
            }
        }
    ];

    movieDataTable = $('#user-movie-table').DataTable({
        "pageLength": 4,
        "processing": true,
        "ordering": false,
        "searching": false,
        "lengthChange": false,
        "serverSide": true,
        "ajax": '/admin/movie-data-table',
        "columns": showColumns,
        // "columnDefs": [
        //     {width: 40, targets: [-1]}
        // ]
    });
};

$(document).ready(function() {
    movieTable();
});