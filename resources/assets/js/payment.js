$(document).ready(function(){

    var date;
    $.ajax({
        type: "POST",
        url: $('#get-session-url').val(),
        success: function(response){
            var bookingId = response;
            // Checking is cookie is present or not.
            if (typeof $.cookie('time' + bookingId) != "undefined" &&
                $.cookie('time' + bookingId) !== null) {
                date = new Date(Date.parse($.cookie('time' + bookingId)));
            } else {
                date = new Date();
                date.setMinutes(date.getMinutes()+10);
                date.setSeconds(60 - date.getSeconds());
                $.cookie('time' + bookingId, date, { expires: date });
            }
            var difference = Math.abs(date.getTime() - new Date().getTime());
            var milliSec = Date.parse($.cookie('time' + bookingId));
            if (difference < 600000){
                var timeout = new Date(milliSec+60000);
                difference += 60000;
            } else {
                var timeout = new Date(milliSec);
            }
            setTimeout(function(){
                bootbox.alert({
                    title: "Sorry, your session has timed out...",
                    message: "Something's not right here. Please try again after a while." +
                    " We appreciate your patience.",
                    callback: function () {
                        window.location.href = '/';
                    }
                })
            }, difference);

            // Update the count down every 1 second
            setInterval(function() {
                // Get todays date and time
                var now = new Date().getTime();

                // Find the distance between now an the count down date
                var distance = timeout - now;

                // Time calculations for minutes and seconds
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                // Output the result in an element with id="time"
                if (minutes < 0) {
                    $("#time").text("0m 0s");
                } else {
                    $("#time").text(minutes + "m " + seconds + "s ");
                }
            }, 1);
        }
    });

    $(document).on('click','#payment-submit', function(e){
        var showId = $('#show-id').val();
        var seat = $('#booked-seats').val();
        var seats = seat.split(" ");
        $.ajax({
            type: "POST",
            url: $('#seat-check-url').val(),
            data: {
                seats: seats,
                showId: showId
            },
            success: function (msg) {
                if (false === msg.result) {
                    bootbox.alert({
                        title: "Alert",
                        message: "<strong>Seats you selected are currently" +
                        " not available. Sorry for the inconvenience!</strong>"
                    })
                } else {
                    $("#card-info").validate({
                        rules: {
                            email: "required",
                            phone: "required",
                            cardNumber: "required",
                            expiryMonth: "required",
                            expiryYear: "required",
                            cvvNumber: "required",
                        },
                        messages: {
                            email: "Please enter your email address.",
                            phone: "Please enter your mobile number.",
                            cardNumber: "Please enter a valid card number.",
                            expiryMonth: "Please enter the card expiry month.",
                            expiryYear: "Please enter the card expiry year.",
                            cvvNumber: "Please enter a valid CVV Code.",
                        },
                        showErrors: function(errorMap, errorList) {
                            $("#error-div").html("");
                            if(errorList.length) {
                                $("#error-div").html(errorList[0]['message']);
                            }
                        }
                    });
                    if ($("#card-info").valid()) {
                        $('#card-info').submit();
                    }
                }
            },
            error: function (error) {
                //
            }
        });
    });

});
