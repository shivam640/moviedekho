$(document).ready(function() {
    $("#flip").click(function() {
        $("#panel").slideToggle("fast");
    });
    $(".show-button").click(function() {
        var showId = $(this).attr('id');
        $('#form input[name=showId]').val(showId);
        $('#form').submit();
    })
});