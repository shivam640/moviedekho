$(document).ready(function(){

    $(document).on('click', '.select', function () {
        $(this).toggleClass('selected');
    });

    $('#proceed').on( 'click', function(e) {
        e.preventDefault();
        var seats = [];
        var showId = $('#id').val();
        $(".selected").each(function() {
            var id = $(this).attr('id');
            seats.push(id);
        });
        if (0 === seats.length) {
            bootbox.alert({ 
                title: "Alert",
                message: "<strong>Select atleast 1 seat!</strong>"
            })
            e.preventDefault();
        } else {
            $.ajax({
                type: "POST",
                url: $('#seat-check-url').val(),
                data: {
                    seats: seats,
                    showId: showId
                },
                success: function (msg) {
                    if (false === msg.result) {
                        e.preventDefault();
                        bootbox.alert({
                            title: "Alert",
                            message: "<strong>Sorry! Some other user currently" +
                            " booked these seats</strong>"
                        })
                    } else {
                        $('#form input[name=showId]').val(showId);
                        $('#form input[name=seats]').val(seats);
                        $('#form').submit();
                    }
                },
                error: function (error) {
                    //
                }
            });
        }
    });

});

