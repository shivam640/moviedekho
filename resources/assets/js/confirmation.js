$(document).ready(function(){

    $('#proceed').on( 'click', function(e) {
        var showId = $('#id').val();
        var seat = $('#seats').val();
        var seats = seat.split(",");
        $.ajax({
            type: "POST",
            url: $('#seat-check-url').val(),
            data: {
                seats: seats,
                showId: showId
            },
            success: function (msg) {
                if (false === msg.result) {
                    e.preventDefault();
                    bootbox.alert({
                        title: "Alert",
                        message: "<strong>Seats you selected are currently" +
                        " not available. Sorry for the inconvenience!</strong>"
                    })
                } else {
                    $.ajax({
                        type: "POST",
                        url: $('#seat-update-url').val(),
                        data: {
                            seats: seats,
                            showId: showId
                        },
                        success: function () {
                            window.location.href = "/payment";
                        },
                        error: function (error) {
                            //
                        }
                    });
                }
            },
            error: function (error) {
                //
            }
        });
    });

});