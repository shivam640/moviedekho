let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/adminpage.js', 'public/js')
    .js('resources/assets/js/homepage.js', 'public/js')
    .js('resources/assets/js/movie.js', 'public/js')
    .js('resources/assets/js/seat.js', 'public/js')
    .js('resources/assets/js/confirmation.js', 'public/js')
    .js('resources/assets/js/payment.js', 'public/js')
    .styles(['resources/assets/css/adminpage.css'], 'public/css/adminpage.css')
    .styles(['resources/assets/css/homepage.css'], 'public/css/homepage.css')
    .styles(['resources/assets/css/login.css'], 'public/css/login.css')
    .styles(['resources/assets/css/seat.css'], 'public/css/seat.css')
    .styles(['resources/assets/css/movie.css'], 'public/css/movie.css')
    .styles(['resources/assets/css/confirmation.css'], 'public/css/confirmation.css')
    .styles(['resources/assets/css/payment.css'], 'public/css/payment.css')
    .styles(['resources/assets/css/error.css'], 'public/css/error.css')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .version();

