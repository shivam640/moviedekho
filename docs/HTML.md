# Coding Guidelines

Date: 1st Feb 2018
Language: HTML

## Indentation
Use 4 spaces for indenting HTML and Blade files

## Document Type
```html
<!DOCTYPE html>
```

## Use Lower Case Element Names
```html
<section>
    <p>This is a paragraph.</p>
</section>
```

## Close All HTML Elements
```html
<section>
    <p>This is a paragraph.</p>
    <p>This is a paragraph.</p>
</section>
```

## Use Lower Case Attribute Names
```html
<div class="menu">
```

## Quote Attribute Values
```html
<table class="striped">
```

## Spaces and Equal Signs
Avoid spaces around equal signs.
```html
<link rel="stylesheet" href="styles.css">
```

## Line length
Try to avoid code lines longer than 120 characters.

## HTML Comments
```html
<!-- This is a comment -->
```
```html
<!--
  This is a long comment example. This is a long comment example.
  This is a long comment example. This is a long comment example.
-->
```
