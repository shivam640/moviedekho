# Moviedekho.com
A web application that provides feature to book movie tickets online. 

# Installation
## Pre-requisite
* Laravel 5.6
* Apache 2.4
* Mysql 5.7
+ PHP 7.1.3 with the following extensions installed
    * OpenSSL PHP Extension
    * PDO PHP Extension
    * Mbstring PHP Extension
    * Tokenizer PHP Extension
    * XML PHP Extension
    * Ctype PHP Extension
    * JSON PHP Extension

## Installation steps
## Apache 2.4
```
sudo apt-get update
sudo apt-get install apache2 apache2-bin
```

## MYSQL 5.7
```
wget http://dev.mysql.com/get/mysql-apt-config_0.8.3-1_all.deb
sudo dpkg -i mysql-apt-config_0.8.3-1_all.deb
sudo apt-get update
sudo apt-get install mysql-server
mysqld --initialize
```

## PHP 7.1
```
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update && sudo apt-get install php7.1
```

Search packages by running
```
sudo apt-cache search php7.1
```
and install respective package by running
```
sudo apt-get install package-name
```
```
sudo apt-get install php7.1-xml php7.1-zip mcrypt php7.1-mcrypt php7.1-mbstring php7.1 curl

sudo phpenmod mcrypt
sudo phpenmod mbstring
```

## Enable mod rewrite
```
sudo a2enmod rewrite
sudo service apache2 restart
```

## GIT
```
sudo apt-get install git
```

## Composer
```
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
```

## NodeJS & NPM
```
sudo apt-get install nodejs
sudo apt-get install npm
```

## Setting up project
Clone repository:
```
git clone https://[username]@bitbucket.org//shivam640/moviedekho.git
```
Install composer packages
```
composer install
```
Install npm packages
```
npm install
```
Run Mix to minify and version JS and CSS.
```
npm run production
```
Run DB Migration
```
php artisan migrate
```

Create a ".env" file by referring to the ".env.example"

## Running
* Create a virtual host configuration file with **.conf** extension.  
Example: **moviedekho.com.conf**
```
sudo nano /etc/apache2/sites-available/[virtual_host_file_name]
```
* Add the following configuration:
```
<VirtualHost *:80>
    ServerAdmin [admin@domain]
    ServerName [domain_name]
    ServerAlias [domain_name]
    DocumentRoot [path_to_project's_public_directory]
    Redirect / https://[domain_name]
</VirtualHost>

<IfModule mod_ssl.c>
    <VirtualHost _default_:443>
        ServerAdmin [admin@domain]
        ServerName [domain_name]
        ServerAlias [domain_name]
        DocumentRoot [path_to_project's_public_directory]
        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
        <Directory [path_to_project's_public_directory]>
            Options FollowSymLinks MultiViews
            AllowOverride All
            Require all granted
        </Directory>
        SSLEngine on
	</VirtualHost>
</IfModule>
```
**Please replace the placeholders with appropriate values.**

* Run the following command to Enable the virtual host
```
sudo a2ensite [virtual_host_file_name]
```
* Restart Apache
```
sudo service apache2 restart
```
* Open Local Hosts file
```
sudo nano /etc/hosts
```
* Add the following to the Local Hosts file
```
127.0.0.1   [domain_name]
```
* Now open a browser, visit your domain and you should see the home page.

## JS and CSS combining, minifying and versioning
This is done by Laravel Mix.
Please check "webpack.mix.js".
Read more about using Mix at: https://laravel.com/docs/5.6/mix

## Crons
Crons are configured at "app/Console/Kernel.php"
Just need to add the following entry to crontab
```
* * * * * php [Absolute path to project root]/artisan schedule:run >> /dev/null 2>&1
```

## Documents
Coding guidelines and other documents can be found in the "docs" directory.

## Deployment
* BitBucket along with Git is being used for Source code management.  
* Any changes made should be committed to its respective feature branch.
* Changes should be merged into dev branch for testing.
* Once all the testings are passed, dev branch is merged into master branch.

* Go to project root:
```
cd /var/www/html/moviedekho/
```
* Make the application go into "Maintenance mode"
```
php artisan down
```
* Check branch
```
git branch
```
The active branch is shown in "Green" with a "*"
Make sure the active branch is master.
If not, checkout to master branch.
```
git checkout master
```
* Pull master branch:
```
git pull origin master
```
* Update environment changes to "moviedekho / .env" file, if any.
* Run migration if any new table has been introduced or an existing was updated.
```
php artisan migrate
```

* Seed the database
```
php artisan db:seed
```
* Update node modules if a new node package was used while implementing the feature.
```
npm install
```
* Update composer if a new php package was used while implementing the feature.
```
composer update
```
* Combine, minify and version CSS and JS.
```
npm run production
```
* Make the application live.
```
php artisan up
```
* Access application via url and test.
