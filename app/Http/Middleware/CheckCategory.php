<?php
/**
 * This middleware checks the role of the user.
 */

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

/**
 * Class CheckCategory
 * @package App\Http\Middleware
 */
class CheckCategory
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     *         Instance of the current HTTP request.
     * @param  \Closure  $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $category = Auth::user()->role_id;
            if (2 === $category) {
                return redirect('/adminpage/view');
            }
        }
        return $next($request);
    }
}
