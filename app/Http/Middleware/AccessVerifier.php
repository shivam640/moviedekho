<?php
/**
 * This middleware checks for user access to a page.
 */

namespace App\Http\Middleware;

use Closure;
use App\Models\Resource;
use App\Models\Permission;
use App\Models\RoleResourcePermission;
use Illuminate\Support\Facades\Auth;

/**
 * Class AccessVerifier
 * @package App\Http\Middleware
 */
class AccessVerifier
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     *         Instance of the current HTTP request.
     * @param  \Closure  $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $url = $request->path();
            $urlArray = explode('/', $url);
            $resource = $urlArray[0];
            $permission = $urlArray[1];

            $roleId = Auth::user()->role_id;
            $resourceId = Resource::where('name', $resource)
                ->select('id')->first()->id;
            $permissionId = Permission::where('name', $permission)
                ->select('id')->first()->id;

            /**Check for authorization*/
            if (RoleResourcePermission::checkAccess(
                $roleId,
                $resourceId,
                $permissionId
            )) {
                return $next($request);
            } else {
                return redirect('/error');
            }
        }
        return $next($request);
    }
}
