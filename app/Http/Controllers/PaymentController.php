<?php
/**
 * This controller contains methods required to show
 * the payment page and to do the payment.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Cartalyst\Stripe\Stripe;
use Stripe\Error\card;
use Config;
use App\Models\Booking;
use App\Models\Show;
use App\Models\BookedSeat;
use App\Mail\BookingSummary;
use Illuminate\Support\Facades\Auth;
use Exception;
use Log;

class PaymentController extends Controller
{
    /**
     * Return payment page view.
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function paymentPage()
    {
        try {
            $bookingId = session('id');
            $booking = Booking::getBooking(
                $bookingId,
                array(
                    'show_id',
                    'seats',
                    'amount'
                )
            );
            $bookedSeatsArray = json_decode($booking->seats);
            $show = Show::getShowInformation($booking->show_id);

            return view('payment')
                ->with('show', $show)
                ->with('showId', $booking->show_id)
                ->with('seats', $bookedSeatsArray)
                ->with('amount', $booking->amount)
                ->with('user', Auth::user());
        } catch (Exception $e) {
            Log::error('Error in paymentPage method of PaymentController: '
                . $e->getMessage());
            abort(500);
        }
    }

    /**
     * Return booking id stored in the session.
     *
     * @return \Symfony\Component\HttpFoundation\Response|
     *         \Illuminate\Contracts\Routing\ResponseFactory
     */
    public function getSession()
    {
        $bookingId = session('id');

        return response()->json($bookingId);
    }

    /**
     * Function to validate card details and do the payment.
     *
     * @param Request $request Instance of the current HTTP request.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function pay(Request $request)
    {
        $bookingId = session('id');
        $request->validate([
            'email' => 'required|E-mail',
            'phone' => 'required',
            'cardNumber' => 'required',
            'cardName' => 'nullable',
            'expiryMonth' => 'required',
            'expiryYear' => 'required',
            'cvvNumber' => 'required'
        ]);
        try {
            $stripe = Stripe::make(env("STRIPE_SECRET"));
            $booking = Booking::getBooking(
                $bookingId,
                array(
                    'show_id',
                    'seats',
                    'amount'
                )
            );
            $show = Show::getShowInformation($booking->show_id);

            /** Generating stripe token */
            $token = $stripe->tokens()->create([
                'card' => [
                    'number'    => $request->cardNumber,
                    'exp_month' => $request->expiryMonth,
                    'exp_year'  => $request->expiryYear,
                    'cvc'       => $request->cvvNumber,
                ],
            ]);

            /** Return back if token not generated. */
            if (!isset($token['id'])) {
                $returnValue = back()
                    ->with(
                        'errorMessage',
                        'The Stripe token was not generated succesfuly.'
                    );
            } else {
                /** Creating a charge. */
                $charge = $stripe->charges()->create([
                    'card' => $token['id'],
                    'currency' => Config::get('constants.CURRENCY'),
                    'amount' => $booking->amount,
                ]);

                if ('succeeded' === $charge['status']) {
                    BookedSeat::updateSeats($booking->show_id, $booking->seats);
                    Booking::updatePayment($bookingId);
                    \Mail::to(Auth::user())->send(new BookingSummary($show, $booking->seats));
                    $request->session()->forget('id');
                    session()->flash(
                        'message',
                        'Your payment process successfully done!
                         The receipt of the booking has been sent to your email.'
                    );
                    $returnValue = redirect('/');
                } else {
                    $returnValue = back()
                        ->with('errorMessage', 'Your payment process not submited.');
                }
            }
        } catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {
            $returnValue = back()->with('errorMessage', $e->getMessage());
        } catch (Exception $e) {
            $returnValue = back()->with('errorMessage', $e->getMessage());
        }

        return $returnValue;
    }
}
