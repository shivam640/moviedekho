<?php
/**
 * This controller provides methods required for the seating management.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Booking;
use App\Models\Show;
use App\Models\BookedSeat;
use Exception;
use Log;

/**
 * Class SeatController
 *
 * @package App\Http\Controllers
 */
class SeatController extends Controller
{
    /**
     * Loading the seat selection page.
     *
     * @param Request $request Instance of the current HTTP request.
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function showSeatPage(Request $request)
    {
        try {
            $showId = $request->showId;
            $bookedSeatsObject = BookedSeat::getBookedSeats(
                $showId,
                array('booked_seats')
            );
            $bookedSeatsArray = [];
            if ($bookedSeatsObject != null) {
                $bookedSeatsArray = json_decode($bookedSeatsObject->booked_seats);
            }
            $show = Show::getShowInformation($showId);
            if (!isset($show)) {
                abort(404, 'Movie not found.');
            }

            return view('seats')
                ->with('bookedSeatsArray', $bookedSeatsArray)
                ->with('show', $show)
                ->with('id', $showId);
        } catch (Exception $e) {
            if (404 === $e->getStatusCode()) {
                Log::error('Error in showMovie method of HomeController: '
                    . $e->getMessage());
                abort(404);
            } else {
                Log::error('Error in showMovie method of HomeController: '
                    . $e->getMessage());
                abort(500);
            }
        }
}

    /**
     * Updating the seats in booked_seats table and adding record to bookings.
     * table
     *
     * @param Request $request  Instance of the current HTTP request.
     *
     * @return \Symfony\Component\HttpFoundation\Response|
     *         \Illuminate\Contracts\Routing\ResponseFactory
     */
    public function updateSeat(Request $request)
    {
        $request->validate([
            'seats' => 'required|array|min:1',
            'showId' => 'integer'
        ]);
        try {
            $id = Booking::store($request);
            session(['id' => $id]);
            $result = true;
        } catch (Exception $e) {
            Log::error('Error in updateSeat method of SeatController: '
                . $e->getMessage());
            $result = false;
        }

        return response()->json(['result' => $result]);
    }

    /**
     * Checks if the seat is blocked by another user.
     *
     * @param Request $request Instance of the current HTTP request.
     *
     * @return \Symfony\Component\HttpFoundation\Response|
     *         \Illuminate\Contracts\Routing\ResponseFactory
     */
    public function checkSeat(Request $request)
    {
        try {
            $result = true;
            $bookedSeatsObject = BookedSeat::getBookedSeats(
                $request->showId,
                array('booked_seats')
            );
            $bookedSeatsArray = [];
            if ($bookedSeatsObject != null) {
                $bookedSeatsArray = json_decode($bookedSeatsObject->booked_seats);
            }

            /** Checks that the seats selected are blocked or not. */
            foreach ($request->seats as $seat) {
                if (in_array($seat, $bookedSeatsArray)) {
                    $result = false;
                }
            }
        } catch (Exception $e) {
            Log::error('Error in checkSeat method of SeatController: '
                . $e->getMessage());
            $result = false;
        }

        return response()->json(['result' => $result]);
    }

    /**
     * Return confirmation view.
     *
     * @param Request $request Instance of the current HTTP request.
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function confirmation(Request $request)
    {
        try {
            $show = Show::getShowInformation($request->showId);

            return view('confirmation')
                ->with('show', $show)
                ->with('seats', $request->seats)
                ->with('id', $request->showId);
        } catch (Exception $e) {
            Log::error('Error in confirmation method of SeatController: '
                . $e->getMessage());
            abort(500);
        }
    }
}
