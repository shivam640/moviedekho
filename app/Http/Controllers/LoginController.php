<?php
/**
 * This controller provides methods for login feature.
 */

namespace App\Http\Controllers;

use Auth;
use Socialite;
use App\Models\User;
use Exception;
use Log;

/**
 * Class LoginController
 *
 * @package App\Http\Controllers
 */
class LoginController extends Controller
{
    /**
     * Function to display login page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginPage()
    {
    	return view('login');
    }

    /**
     * Checks user authentication and login.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function attemptLogin()
    {
    	if (! auth()->attempt(request(['email', 'password']))) {
    		return back()->withErrors([
    			'message' => 'Please check your credentials and try again.'
    		]);
    	}

    	return redirect('/');
    }

    /**
     * Logout the user
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout()
    {
    	auth()->logout();

    	return redirect('/');
    }

    /**
     * Redirect the user to the OAuth Provider.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that 
     * redirect them to the authenticated users homepage.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function handleProviderCallback($provider)
    {
        try {
            $user = Socialite::driver($provider)->user();

            $authUser = $this->findOrCreateUser($user, $provider);
            Auth::login($authUser, true);

            return redirect('/');
        } catch (Exception $e) {
            Log::error('Error in handleProviderCallback method of LoginController: '
                . $e->getMessage());
            abort(500);
        }
    }

    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     *
     * @param  $user Socialite user object.
     * @param $provider Social auth provider.
     *
     * @return  User
     */
    public function findOrCreateUser($user, $provider)
    {
        try {
            $authUser = User::where('email', $user->email)->first();
            if ($authUser) {
                return $authUser;
            }
            return User::create([
                'name' => $user->name,
                'email' => $user->email
            ]);
        } catch (Exception $e) {
            Log::error('Error in findOrCreateUser method of LoginController: '
                . $e->getMessage());
            abort(500);
        }
    }
}
