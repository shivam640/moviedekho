<?php
/**
 * This controller provides methods required for the registration.
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Exception;
use Log;

/**
 * Class RegistrationController.
 *
 * @package App\Http\Controllers
 */
class RegistrationController extends Controller
{
    /**
     * Function to display signup page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showRegistrationPage()
    {
    	return view('signup');
    }

    /**
     * Storing user record to the database.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function addUser()
    {
        $this->validate(request(), [
            'name' => 'required',
            'email' => 'required|E-mail|unique:users',
            'password' => 'required|min:8',
            'phone' => 'required|digits:10'
        ]);

        try {
            $user = User::create([
                'name' => request('name'),
                'email' => request('email'),
                'password' => Hash::make(request('password')),
                'phone' => request('phone')
            ]);
            auth()->login($user);
            session()->flash('message', 'Thanks so much for signing up!');
        } catch (Exception $e) {
            Log::error('Error in addUser method of AdminController: '
                . $e->getMessage());
            session()->flash(
                'error',
                'Their was an error while registering. Please try again later!'
            );
        }

        return redirect('/');
    }
}
