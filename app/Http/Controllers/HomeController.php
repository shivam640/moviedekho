<?php
/**
 * This controller contains methods required to show
 * the home page and the selected movie page.
 */

namespace App\Http\Controllers;

use App\Models\Movie;
use App\Models\Show;
use Carbon\Carbon;
use Exception;
use Log;

/**
 * Class HomeController
 *
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Return homepage view with all movies data.
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory|void
     */
    public function showHomePage()
    {
        try {
            $movies = Movie::getCorouselMovies(
                array(
                    'poster'
                )
            );

            return view('homepage')->with('movies', $movies);
        } catch (Exception $e) {
            Log::error('Error in showHomePage method of HomeController: '
                . $e->getMessage());
            abort(500);
        }
    }

    /**
     * Show details of show related to the movie.
     *
     * @param string $id   Id of the selected movie.
     * @param string $date Date of the current date.
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function showMovie($id, $date)
    {
        try {
            $start = Carbon::now();
            $dates = array(Carbon::parse($start)->format('Y-m-d'));
            for ($i = 1; $i < 7; $i++) {
                array_push($dates, Carbon::parse($start->addDays(1))
                    ->format('Y-m-d'));
            }
            $movie = Movie::getMovie(
                $id,
                array(
                    'id',
                    'movie_name',
                    'duration',
                    'poster',
                    'director',
                    'cast',
                    'description'
                )
            );
            if (!isset($movie)) {
                abort(404, 'Movie not found.');
            }
            if (!in_array($date, $dates)) {
                $date = date_format(Carbon::now(),"Y-m-d");
                return redirect('/movie/details/' . $id . '/' . $date);
            }
            $shows =  Show::getShowsUsingMovieIdForADate($id, $date);

            return view('movie')
                ->with('movie', $movie)
                ->with('shows', $shows)
                ->with('dates', $dates)
                ->with('presentTime', Carbon::now())
                ->with('selectedDate', $date);
        } catch (Exception $e) {
            if (404 === $e->getStatusCode()) {
                Log::error('Error in showMovie method of HomeController: '
                    . $e->getMessage());
                abort(404);
            } else {
                Log::error('Error in showMovie method of HomeController: '
                    . $e->getMessage());
                abort(500);
            }
        }
    }

}
