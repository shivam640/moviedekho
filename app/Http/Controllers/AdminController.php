<?php
/**
 * This controller provides methods required for the admin features.
 */

namespace App\Http\Controllers;

use App\Models\Movie;
use App\Models\Screen;
use App\Models\Theatre;
use App\Models\Show;
use Illuminate\Http\Request;
use App\Http\Requests\TheatreRequest;
use Exception;
use Log;
use App\Helpers\ModelTableDataCommon;
use DB;

/**
 * Class AdminController.
 *
 * @package App\Http\Controllers
 */
class AdminController extends Controller
{
    /**
     * Show admin page view.
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function showAdminPage()
    {
        try {
            return view('admin');
        } catch (Exception $e) {
            Log::error('Error in showAdminPage method of AdminController: '
                . $e->getMessage());
            abort(500);
        }
    }

    /**
     * Gets records for show data tables.
     *
     * @param Request $request  Instance of the current HTTP request.
     *
     * @return array
     */
    public function getShowsForDatatable(Request $request)
    {
        return ModelTableDataCommon::tableData(
            $request, 'Show'
        );
    }

    /**
     * Gets records for movie data tables.
     *
     * @param Request $request  Instance of the current HTTP request.
     *
     * @return array
     */
    public function getMoviesForDatatable(Request $request)
    {
        return ModelTableDataCommon::tableData(
            $request, 'Movie'
        );
    }

    /**
     * Gets records for theatre data tables.
     *
     * @param Request $request  Instance of the current HTTP request.
     *
     * @return array
     */
    public function getTheatresForDatatable(Request $request)
    {
        return ModelTableDataCommon::tableData(
            $request, 'Theatre'
        );
    }

    /**
     * Loads the movie modal.
     *
     * @param null|string $id  Movie id.
     *
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function loadMovieModalForm($id = null)
    {
        try {
            if (!empty($id)) {
                $movie = Movie::getMovie(
                    $id,
                    array(
                        'movie_name',
                        'duration',
                        'poster',
                        'director',
                        'cast',
                        'description'
                    )
                );

                return View('modals.movie')
                    ->with('movie', $movie);
            } else {
                return View('modals.movie');
            }
        } catch (Exception $e) {
            Log::error('Error in loadMovieModalForm method of
             AdminController: '
                . $e->getMessage());
            abort(500);
        }
    }

    /**
     * Adding a movie.
     *
     * @param Request $request  Instance of the current HTTP request.
     *
     * @return \Symfony\Component\HttpFoundation\Response|
     *         \Illuminate\Contracts\Routing\ResponseFactory
     */
    public function addMovie(Request $request)
    {
        $this->validate(request(), [
            'movieName' => 'required',
            'duration' => 'required|numeric',
            'director' => 'string|nullable',
            'cast' => 'string|nullable',
            'description' => 'string|nullable',
            'poster' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        try {
            $result = Movie::store($request);
        } catch (Exception $e) {
            Log::error('Error in addMovie method of AdminController: '
            . $e->getMessage());
            $result = false;
        }

        return response()->json(['result' => $result]);
    }

    /**
     * Editing movie details.
     *
     * @param Request $request Instance of the current HTTP request.
     *
     * @return \Symfony\Component\HttpFoundation\Response|
     *         \Illuminate\Contracts\Routing\ResponseFactory
     */
    public function editMovie(Request $request)
    {
        $this->validate(request(), [
            'movieName' => 'required',
            'duration' => 'required|numeric',
            'director' => 'string|nullable',
            'cast' => 'string|nullable',
            'description' => 'string|nullable'
        ]);

        try {
            $result = Movie::edit($request);
        } catch (Exception $e) {
            Log::error('Error in editMovie method of AdminController: '
                . $e->getMessage() . 'in line no.: ' . $e->getLine());
            $result = false;
        }

        return response()->json(['result' => $result]);
    }

    /**
     * Deleting the movie.
     *
     * @param Request $request Instance of the current HTTP request.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteMovie(Request $request)
    {
        try {
            $result = Movie::movieDelete($request->id);
        } catch (Exception $e) {
            Log::error('Error in deleteMovie method of AdminController: '
                . $e->getMessage());
            $result = false;
        }

        return response()->json(['result' => $result]);
    }

    /**
     * Loads the theatre modal.
     *
     * @param null|string $id  Theatre id.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function loadTheatreModalForm($id = null)
    {
        try {
            if (!empty($id)) {
                $theatre = Theatre::getTheatre($id, ['theatre_name', 'city', 'address', 'state']);
                $screens = Screen::getScreens($id, ['id', 'screen_name', 'capacity']);

                return View('modals.theatre', compact('theatre', 'screens'));
            } else {
                return View('modals.theatre');
            }
        } catch (Exception $e) {
            Log::error('Error in loadTheatreModalForm method of
             AdminController: '
                . $e->getMessage());
            abort(500);
        }
    }

    /**
     * Adding a theatre and screens.
     *
     * @param TheatreRequest $request  Instance of the current Theatre request.
     *
     * @return \Symfony\Component\HttpFoundation\Response|
     *         \Illuminate\Contracts\Routing\ResponseFactory
     */
    public function addTheatre(TheatreRequest $request)
    {
        DB::beginTransaction();

        try {
            if ($id = Theatre::store($request)) {
                $result = Screen::store($request, $id);
                if (!$result) {
                    DB::rollback();
                }
            } else {
                DB::rollback();
                $result = false;
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            Log::error('Error in addTheatre method of AdminController: '
                . $e->getMessage());
            $result = false;
        }

        return response()->json(['result' => $result]);
    }

    /**
     * Editing theatre and screen.
     *
     * @param TheatreRequest $request  Instance of the current Theatre request.
     *
     * @return \Symfony\Component\HttpFoundation\Response|
     *         \Illuminate\Contracts\Routing\ResponseFactory
     */
    public function editTheatre(TheatreRequest $request)
    {
        DB::beginTransaction();

        try {
            if (Theatre::edit($request)) {
                $result = Screen::edit($request);
                if (!$result) {
                    DB::rollback();
                }
            } else {
                DB::rollback();
                $result = false;
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            Log::error('Error in editTheatre method of AdminController: '
                . $e->getMessage() . 'in line no. ' . $e->getLine());
            $result = false;
        }

        return response()->json(['result' => $result]);
    }

    /**
     * Deleting the theatre.
     *
     * @param Request $request Instance of the current HTTP request.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteTheatre(Request $request)
    {
        try {
            $result = Theatre::theatreDelete($request->id);
        } catch (Exception $e) {
            Log::error('Error in deleteTheatre method of AdminController: '
                . $e->getMessage());
            $result = false;
        }

        return response()->json(['result' => $result]);
    }

    /**
     * Loads show modal with the existing movies and theatres.
     *
     * @param null|string $id Show id.
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function loadShowModalForm($id = null)
    {
        try {
            $movies = Movie::getAllMovies(
                array('id', 'movie_name')
            );
            $theatres = Theatre::getAllTheatres(
                array('id', 'theatre_name')
            );
            $movieArray = [];
            foreach ($movies as $movie) {
                $movieArray[$movie->id] = $movie->movie_name;
            }
            $theatreArray = [];
            foreach ($theatres as $theatre) {
                $theatreArray[$theatre->id] = $theatre->theatre_name;
            }
            $screenArray = [];

            if (!empty($id)) {
                $show = Show::getShow(
                    $id,
                    array('screen_id','movie_id','time','date')
                );
                $screenId = $show->screen_id;
                $theatre = Screen::getScreen($screenId, array('theatre_id'));
                $screens = Screen::getScreensUsingTheatreId(
                    $theatre->theatre_id,
                    array('id', 'screen_name')
                );
                foreach ($screens as $screen) {
                    $screenArray[$screen->id] = $screen->screen_name;
                }

                return View('modals.show')
                    ->with('movieArray', $movieArray)
                    ->with('theatreArray', $theatreArray)
                    ->with('screenArray', $screenArray)
                    ->with('show', $show)
                    ->with('theatre', $theatre);
            } else {
                return View('modals.show')
                    ->with('movieArray', $movieArray)
                    ->with('theatreArray', $theatreArray)
                    ->with('screenArray', $screenArray);
            }
        } catch (Exception $e) {
            Log::error('Error in loadShowModalForm method of AdminController: '
                . $e->getMessage());
            abort(500);
        }
    }

    /**
     * Loads screens dynamically after selecting theatre.
     *
     * @param Request $request  Instance of the current HTTP request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function loadScreens(Request $request)
    {
        try {
            $screens = Screen::getScreensUsingTheatreId(
                $request->input( 'theatreId' ),
                array('id', 'screen_name')
            );
        } catch (Exception $e) {
            Log::error('Error in loadScreens method of AdminController: '
                . $e->getMessage());
            $screens = false;
        }

        return response()->json($screens);
    }

    /**
     * Adding shows.
     *
     * @param Request $request  Instance of the current HTTP request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addShow(Request $request)
    {
        $this->validate(request(), [
            'theatre' => 'required',
            'screen' => 'required',
            'movie' => 'required',
            'time' => 'required',
            'date' => 'required'
        ]);

        try {
            $result = Show::store($request);
        } catch (Exception $e) {
            Log::error('Error in addShow method of AdminController: '
                . $e->getMessage());
            $result = false;
        }

        return response()->json(['result' => $result]);
    }

    /**
     * Editing Show.
     *
     * @param Request $request Instance of the current HTTP request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function editShow(Request $request)
    {
        $this->validate(request(), [
            'theatre' => 'required',
            'screen' => 'required',
            'movie' => 'required',
            'time' => 'required',
            'date' => 'required',
            'id' => 'required'
        ]);

        try {
            $result = Show::edit($request);
        } catch (Exception $e) {
            Log::error('Error in editShow method of AdminController: '
                . $e->getMessage());
            $result = false;
        }

        return response()->json(['result' => $result]);
    }

    /**
     * Deleting the show.
     *
     * @param Request $request Instance of the current HTTP request.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteShow(Request $request)
    {
        try {
            $result = Show::showDelete($request->id);
        } catch (Exception $e) {
            Log::error('Error in deleteShow method of AdminController: '
                . $e->getMessage());
            $result = false;
        }

        return response()->json(['result' => $result]);
    }
}
