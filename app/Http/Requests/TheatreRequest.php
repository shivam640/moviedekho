<?php
/**
 * This request is for validating theatre information.
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class TheatreRequest
 * @package App\Http\Requests
 */
class TheatreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'theatreName' => 'required',
            'address' => 'required|string',
            'city' => 'required|string',
            'state' => 'required|string'
        ];

        foreach($this->request->get('screens') as $key => $val)
        {
            $rules['screens.'.$key] = 'required';
            $rules['capacity.'.$key] = 'required';
        }

        return $rules;
    }
}
