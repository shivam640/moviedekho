<?php
namespace App\Helpers;
use DB;
use Illuminate\Http\Request;

/**
 * Class ModelTableDataCommon
 * @package App\Helpers
 */
class ModelTableDataCommon
{

    /**
     * @param Request $request
     * @param string $model
     *
     * @return array
     */
    public static function tableData(Request $request, $model = '')
    {
        if ('' == $model){
            abort(404);
        }

        $dataTableParam = $request->all();

        $model =  '\App\Models\\' . $model;
        $model_obj = new $model;

        if (method_exists($model_obj, 'getModelData')) {
            $select = $model_obj->getModelData();
            $selectNoLimit = $model_obj->getModelData();
        }

        if (method_exists($model_obj, 'getTableAliasColumns')){
            $allFields = $model_obj->getTableAliasColumns();
        } else {
            $allFields = $model_obj->getTableColumns();
        }

        return DataTableCommon::attachDataTableQuery(
            $dataTableParam, $select, $selectNoLimit, $allFields
        );
    }
}