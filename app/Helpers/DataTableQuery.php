<?php
namespace App\Helpers;
use DB;

/**
 * Class DataTableQuery
 * @package App\Helpers
 */
class DataTableQuery
{
    /**
     * @function setLimit()
     * @purpose functionality to set the limit
     * @param $select
     * @param $datableParam
     * @return $select the db tabel odbject
     */
    public function setLimit($select, $datableParam)
    {
        if (isset($datableParam['start']) && isset($datableParam['length'])) {
            //For pagination adding limit
            $start = $datableParam['start'];
            $limit = $datableParam['length'];

            //adding limit
            if (isset($start) && ($limit != '-1')) {
                $select->skip($start)->take($limit);
            }
        }
        return $select;
    }

    /**
     * @function setOrder()
     * @purpose functionality to set the order
     * @param $select
     * @param $datableParam
     * @param $allFields fields to be listed
     * @param $defaultSortCol, if default sorting column is passed
     * @return $select the db tabel odbject
     */
    public function setOrder($select, $datableParam, $allFields, $defaultSortCol = '', $defaultSortOrder = '')
    {

        try {
            //Ordering
            // gate to check if defult sort order is to apply
            $flag = true;
            if (isset($datableParam['order'])) {
                foreach ($datableParam['order'] as $index => $value) {
//                    dd($value);
                    if ($datableParam['columns'][$index]['orderable'] == "true") {
                        $flag = false;
                        if ($allFields) {
                            $select->orderByRaw(
                                DB::RAW($allFields[$value['column']] . ' ' . $value['dir'])
                            );
                        }
                    }
                }
                if ($flag && $defaultSortCol) {
                    $select->orderBy($allFields[$defaultSortCol], $defaultSortOrder);
                }
            }
            return $select;
        } catch(Exception $e) {
            error_log($e->getMessage());
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param $datableParam
     * @param $allFields
     *
     * @return array
     */
    public function getWhere($datableParam, $allFields)
    {
        $groupFunctionPresent = 0;
        $sWhere = ''; $sHaving = ''; $whereHavingArray = [];

        //It will add where condition for those which are
        if (isset($datableParam['search']['value']) && !empty($datableParam['search']['value']) ) {

            //Escaping search strings
            $searchData =  trim(DB::getPdo()->quote($datableParam['search']['value']), "'");

            foreach ($allFields as $index => $value) {
                if (strpos(strtolower($value), 'group_concat') !== false) {
                    $groupFunctionPresent = 1;
                    break;
                }
            }

            if (0 === $groupFunctionPresent) {
                foreach ($allFields as $index => $value) {
                    if ($datableParam['columns'][$index]['searchable'] == "true") {
                        $sWhere .= $allFields[$index] . ' LIKE "%' . $searchData . '%" OR ';
                    }
                }
                //remove the OR of the last
                $sWhere = substr_replace($sWhere, "", -3);
                $whereHavingArray['where'] = $sWhere;
            } else {
                foreach ($allFields as $index => $value) {
                    if ($datableParam['columns'][$index]['searchable'] == "true") {
                        $sHaving .= $allFields[$index] . ' LIKE "%' . $searchData . '%" OR ';
                    }
                }
                //remove the OR of the last
                $sHaving = substr_replace($sHaving, "", -3);
                $whereHavingArray['having'] = $sHaving;
            }
        }
        return $whereHavingArray;
    }


    /**
     * @function prepareDtResponse()
     * @purpose functionality to prepare dt response in normal
     * @param $select query
     * @param $selectNoLimit query without limit
     * @param $adapter to run the query
     * @param $datableParam
     * @return array the db table response
     */
    public function prepareDtResponse($datableParam, $select, $selectNoLimit)
    {
        // get results
        try {
            $result = $select->get();

            $totalDisplayRecordsCount = $selectNoLimit->get()->count();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        //Initializing the output
        $output = array(
            "draw" => isset($datableParam['draw']) ? intval($datableParam['draw']) : 0,
            "recordsTotal" => 0,
            "recordsFiltered" => 0,
            "data" => array(),
        );

        //If getting any data
        if (isset($result) && (!empty($result))) {
            $currentRowsCount = count($result);

            $output['recordsTotal'] = $currentRowsCount;
            $output['recordsFiltered'] = $totalDisplayRecordsCount;

            // If results then count and sort else return empty
            if ($currentRowsCount > 0) {
                //converting to array
                $currentData = $result;
                //initializing an array for output
                $data = array();

                //Put all the data in a array to print outside
                foreach ($currentData as $rowData) {
                    $row = array();

                    //Put each records of the row
                    foreach ($rowData as $key => $colData) {
                        //Formating the records
                        $row[$key] = htmlentities($colData);
                    }
                    //Put the data in the output array
                    $data[] = $row;
                }
                //add the output data in the array list
                $output['data'] = $data;
            }
        }
        return $output;
    }
}