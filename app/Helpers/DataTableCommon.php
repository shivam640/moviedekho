<?php
namespace App\Helpers;
use App\Helpers\DataTableQuery;
use DB;
use Illuminate\Support\Facades\Log;

/**
 * Class DataTableCommon
 * @package App\Helpers
 */
class DataTableCommon
{
    /**
     * @param $datableParam  Param from data-table.
     * @param $select        Select query.
     * @param $selectNoLimit SelectLimit query.
     * @param $allFields     All field which is to be search and order.
     *
     * @return array
     */
    public static function attachDataTableQuery($datableParam, $select, $selectNoLimit, $allFields) 
    {
        $dataTable = new DataTableQuery();
        $sWhere = $dataTable->getWhere($datableParam, $allFields);
        //If above where condition is set
        if (isset($sWhere['where']) && !empty($sWhere['where'])) {
            $select->whereRaw('(' . $sWhere['where'] . ')');

            //for all the records
            $selectNoLimit->whereRaw('(' . $sWhere['where'] . ')');

        }

        if (isset($sWhere['having']) && !empty($sWhere['having'])) {
            $select->havingRaw('(' . $sWhere['having'] . ')');

            //for all the records
            $selectNoLimit->havingRaw('(' . $sWhere['having'] . ')');
        }

        $select = $dataTable->setLimit($select, $datableParam);
        $select = $dataTable->setOrder($select, $datableParam, $allFields);

        return $dataTable->prepareDtResponse($datableParam, $select, $selectNoLimit);
    }
}