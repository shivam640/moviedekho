<?php
/**
 * This class is for sending mail about show information to user.
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;

/**
 * Class BookingSummary
 * @package App\Mail
 */
class BookingSummary extends Mailable
{
    use Queueable, SerializesModels;

    protected $show;
    protected $seats;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($show, $seats)
    {
        $this->show = $show;
        $this->seats = $seats;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $seatArray = json_decode($this->seats);
        $bookedSeats = implode(",", $seatArray);
        return $this->from('admin@moviedekho.com')
            ->view('emails.booking')
            ->with([
                'movieName' => $this->show->movie_name,
                'theatreName' => $this->show->theatre_name,
                'address' => $this->show->address,
                'screenName' => $this->show->screen_name,
                'showTime' => $this->show->time,
                'showDate' => $this->show->date,
                'userName' => Auth::user()->name,
                'emailId' => Auth::user()->email,
                'seats' => $bookedSeats,
            ]);
    }
}
