<?php
/**
 * This model provides methods to store and edit shows.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DatePeriod;
use DateTime;
use DateInterval;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * Class Show
 *
 * @package App
 *
 * @author shivam.gupta@mindfiresolutions
 */
class Show extends Model
{
    /**
     * The attributes that are used display.
     *
     * @var array
     */
    protected $columns = [
        'theatre_name', 'screen_name','date', 'time',  'movie_name',
    ];

    /**
     * Storing record to shows table.
     *
     * @param Request $request Instance of the current HTTP request.
     * @throws \Exception
     *
     * @return void
     */
    public static function store($request)
    {
        try {
            $date = explode(" - ",$request->date);
            $start = $date[0];
            $end = $date[1];
            $dates = array();
            $period = new DatePeriod(
                new DateTime($start),
                new DateInterval('P1D'),
                new DateTime($end)
            );
            foreach ($period as $key => $value) {
                array_push($dates, $value->format('Y-m-d'));
            }
            array_push($dates, date('Y-m-d', strtotime($end)));
            $data = [];
            foreach ($dates as $date) {
                array_push($data, array(
                    'screen_id'=> $request->screen,
                    'movie_id'=> $request->movie,
                    'date' => $date,
                    'time' => $request->time,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ));
            }

            return Show::insert($data);
        } catch(Exception $e) {
            Log::error(
                'Error in store method of Show model: '
                . $e->getMessage());

            return false;
        }
    }

    /**
     * Editing the show.
     *
     * @param Request $request  Instance of the current HTTP request.
     *
     * @return void
     */
    public static function edit($request)
    {
        $date = strtr($request->date, '/', '-');
        $NewDate = date('Y-m-d', strtotime($date));
        try {
            $show = Show::find($request->id);
            $show->screen_id = $request->screen;
            $show->movie_id = $request->movie;
            $show->date = $NewDate;
            $show->time = $request->time;
            return $show->update();
        } catch(Exception $e) {
            Log::error(
                'Error in edit method of Show model: '
                . $e->getMessage());
            return false;
        }
    }

    /**
     * Fetch all shows record.
     *
     * @return mixed
     */
    public static function getAllShows()
    {
        try {
            return DB::table('shows')
                ->join('screens', 'shows.screen_id', '=', 'screens.id')
                ->join('movies', 'shows.movie_id', '=', 'movies.id')
                ->select('shows.*', 'screens.screen_name', 'movies.movie_name')
                ->get();
        } catch(Exception $e) {
            Log::error(
                'Error in getAllShows method of Show model: '
                . $e->getMessage());
            abort(500);
        }
    }

    /**
     * Fetches a show record of given show id.
     *
     * @param integer $id    Id of the show to be fetched.
     * @param array $columns Array of all the columns required.
     *
     * @return mixed
     */
    public static function getShow($id, $columns)
    {
        try {
            return Show::select($columns)->find($id);
        } catch(Exception $e) {
            Log::error(
                'Error in getShow method of Show model: '
                . $e->getMessage());
            abort(500);
        }
    }

    /**
     * Fetches a show record of the selected date using movie id.
     *
     * @param string $id    Movie id.
     * @param string $date  Selected Date.
     *
     * @return mixed
     */
    public static function getShowsUsingMovieIdForADate($id, $date)
    {
        try {
            return DB::table('shows')
                ->select('theatres.theatre_name', 'theatres.address',
                    DB::raw('group_concat(shows.id,"|",shows.time SEPARATOR ",") as times'))
                ->join('screens', 'shows.screen_id', '=', 'screens.id')
                ->join('theatres', 'screens.theatre_id', '=', 'theatres.id')
                ->where('shows.movie_id', $id)
                ->where('shows.date', $date)
                ->groupBy('theatres.theatre_name', 'theatres.address')
                ->get();
        } catch(Exception $e) {
            Log::error(
                'Error in getShowsUsingMovieIdForADate method of Show model: '
                . $e->getMessage());
            abort(500);
        }
    }

    /**
     * Fetches the show information with the given show id.
     *
     * @param integer $id Show id.
     *
     * @return mixed
     */
    public static function getShowInformation($id)
    {
        try {
            return DB::table('shows')
                ->join('screens', 'shows.screen_id', '=', 'screens.id')
                ->join('theatres', 'screens.theatre_id', '=', 'theatres.id')
                ->join('movies', 'shows.movie_id', '=', 'movies.id')
                ->select(
                    'theatres.theatre_name',
                    'theatres.address',
                    'movies.movie_name',
                    'screens.screen_name',
                    'shows.date',
                    'shows.time'
                )
                ->where('shows.id', $id)
                ->first();
        } catch(Exception $e) {
            Log::error(
                'Error in getShowsUsingMovieIdForADate method of Show model: '
                . $e->getMessage());
            abort(500);
        }
    }

    /**
     * Deletes a record of given show id.
     *
     * @param string $id  Id of the record to be deleted.
     *
     * @return bool|int
     */
    public static function showDelete($id)
    {
        try {
            return Show::destroy($id);
        } catch(Exception $e) {
            Log::error(
                'Error in showDelete method of Show model: '
                . $e->getMessage());

            return false;
        }
    }

    /**
     * Get table columns for data tables.
     *
     * @return array
     */
    public function getTableColumns()
    {
        return $this->columns;
    }

    /**
     * Returns data to for the data table.
     *
     * @return mixed
     */
    public function getModelData()
    {
        return DB::table('shows')
            ->select('shows.id as DT_RowId', 'shows.date', 'shows.time', 'screens.screen_name',
                'movies.movie_name', 'theatres.theatre_name')
            ->join('screens', 'shows.screen_id', '=', 'screens.id')
            ->join('movies', 'shows.movie_id', '=', 'movies.id')
            ->join('theatres', 'screens.theatre_id', '=', 'theatres.id');
    }
}
