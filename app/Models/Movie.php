<?php
/**
 * This model provides methods to store and edit movies.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Log;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class Movie
 *
 * @package App
 */
class Movie extends Model
{
    protected $fillable = [
        'theatre_name', 'address', 'city', 'state'
    ];

    /**
     * The attributes that are used display.
     *
     * @var array
     */
    protected $columns = [
        'movie_name', 'duration', 'director',  'cast', 'description', 'poster'
    ];

    /**
     * Storing record to movies table.
     *
     * @param Request $request  Instance of the current HTTP request.
     *
     * @return void
     */
    public static function store($request)
    {
        try {
            $path = $request->file('poster')->store('public');

            $movie = new Movie;
            $movie->movie_name = $request->movieName;
            $movie->duration = $request->duration;
            $movie->poster = $path;
            $movie->director = $request->director;
            $movie->cast = $request->casts;
            $movie->description = $request->description;

            return $movie->save();
        } catch(Exception $e) {
            Log::error(
                'Error in store method of Movie model: '
                . $e->getMessage());

            return false;
        }
    }

    /**
     * Editing records of movies table.
     *
     * @param Request $request  Instance of the current HTTP request.
     *
     * @return void
     */
    public static function edit($request)
    {
        try {
            $movie = Movie::find($request->movieId);
            $movie->movie_name = $request->movieName;
            $movie->duration = $request->duration;
            $movie->director = $request->director;
            $movie->cast = $request->casts;
            $movie->description = $request->description;

            if (isset($request->poster)) {
                if (!empty($movie->poster)) {
                    Storage::delete($movie->poster);
                }
                $movie->poster = $request->file('poster')->store('public');
            }

            return $movie->update();
        } catch(Exception $e) {
            Log::error(
                'Error in edit method of Movie model: '
                . $e->getMessage());

            return false;
        }
    }

    /**
     * Fetches all movies record.
     *
     * @param array $columns  Array of all the columns required.
     *
     * @return mixed
     */
    public static function getAllMovies($columns)
    {
        try {
            return Movie::get($columns);
        } catch(Exception $e) {
            Log::error(
                'Error in getAllMovies method of Movie model: '
                . $e->getMessage());
            abort(500);
        }
    }

    /**
     * Fetches movies for corousel.
     *
     * @param array $columns  Array of all the columns required.
     *
     * @return mixed
     */
    public static function getCorouselMovies($columns)
    {
        try {
            return Movie::latest()->limit(5)->get($columns);
        } catch(Exception $e) {
            Log::error(
                'Error in getAllMovies method of Movie model: '
                . $e->getMessage());
            abort(500);
        }
    }

    /**
     * Fetches a record of the given movie id.
     *
     * @param string $id      Movie id.
     * @param array $columns  array containing the required columns to be fetched.
     *
     * @return object
     */
    public static function getMovie($id, $columns)
    {
        try {
            return Movie::select($columns)->find($id);
        } catch(Exception $e) {
            Log::error(
                'Error in getMovie method of Movie model: '
                . $e->getMessage());
            abort(500);
        }
    }

    /**
     * Delete the movie record of the given id.
     *
     * @param string $id  Id of the movie record to be deleted.
     *
     * @return bool|int
     */
    public static function movieDelete($id)
    {
        try {
            $movie = Movie::getMovie(
                $id,
                array('poster')
            );
            if (isset($movie)) {
                Storage::delete($movie->poster);
                Movie::destroy($id);

                return true;
            } else {
                return false;
            }
        } catch(Exception $e) {
            Log::error(
                'Error in movieDelete method of Movie model: '
                . $e->getMessage());

            return false;
        }
    }

    /**
     * Get table columns for data tables.
     *
     * @return array
     */
    public function getTableColumns()
    {
        return $this->columns;
    }

    /**
     * Returns data to for the data table.
     *
     * @return mixed
     */
    public function getModelData()
    {
        return DB::table('movies')
            ->select('id as DT_RowId', 'movie_name', 'duration', 'poster',
                'director', 'cast', 'description');
    }
}
