<?php
/**
 * This model provides method for checking user access to a page.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Log;
use Exception;

/**
 * Class RoleResourcePermission
 *
 * @package App
 * @author shivam.gupta@mindfiresolutions
 */
class RoleResourcePermission extends Model
{
    protected $table = "role_resource_permissions";

    /**
     * Check for the record in the role_resource_permissions table.
     *
     * @param integer $roleId       role id of the user.
     * @param integer $resourceId   resource id.
     * @param integer $permissionId permission id.
     *
     * @return mixed
     */
    public static function checkAccess($roleId, $resourceId, $permissionId)
    {
        try {
            return RoleResourcePermission::where([
                ['role_id', $roleId],
                ['resource_id', $resourceId],
                ['permission_id', $permissionId]
            ])
                ->first();
        } catch(Exception $e) {
            Log::error(
                'Error in checkAccess method of RoleResourcePermission model: '
                . $e->getMessage());
        }
    }
}
