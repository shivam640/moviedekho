<?php
/**
 * This model provides methods to store and edit screens.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Integer;

/**
 * Class Screen
 *
 * @package App
 */
class Screen extends Model
{
    /**
     * The attributes that are used display.
     *
     * @var array
     */
    protected $columns = [
        'theatre_name', 'address', 'city', 'state', 'screen_name', 'capacity',
    ];

    public function theatre()
    {
        return $this->belongsTo('App\Models\Theatre');
    }

    /**
     * Storing record to screens table.
     *
     * @param Request $request Instance of the current HTTP request.
     * @param Integer $id      Theatre id.
     *
     * @return void
     */
    public static function store($request, $id)
    {
        try {
            $screens = $request->screens;
            $capacities = $request->capacity;

            foreach (array_combine($screens, $capacities)
                     as $hall => $capacity) {
                $screen = new Screen;
                $screen->theatre_id = $id;
                $screen->screen_name = $hall;
                $screen->capacity = $capacity;
                $screen->save();
            }

            return true;
        } catch(Exception $e) {
            Log::error(
                'Error in store method of Movie model: '
                . $e->getMessage());

            return false;
        }
    }

    /**
     * Updating screens record.
     *
     * @param Request $request  Instance of the current HTTP request.
     *
     * @return void
     */
    public static function edit($request)
    {
        $screens = $request->screens;
        $capacities = $request->capacity;
        try {
            foreach ($screens as $key => $screenName) {
                $screen = Screen::find($key);
                $screen->screen_name = $screenName;
                $screen->capacity = (int)$capacities[$key];
                $screen->update();
            }

            return true;
        } catch(Exception $e) {
            Log::error(
                'Error in edit method of Screen model: '
                . $e->getMessage());

            return false;
        }
    }

    /**
     * Fetches a screen record with the given id.
     *
     * @param integer $id     Id of the screen to be fetched.
     * @param array $columns  Array of all the columns required.
     *
     * @return mixed
     */
    public static function getScreen($id, $columns)
    {
        try {
            return Screen::select($columns)->find($id);
        } catch(Exception $e) {
            Log::error(
                'Error in getScreen method of Screen model: '
                . $e->getMessage());
            abort(500);
        }
    }

    /**
     * Fetches a screen record with the given theatre id.
     *
     * @param $theatreId  Id of the screen to be fetched.
     * @param $columns    Array of all the columns required.
     *
     * @return mixed
     */
    public static function getScreens($theatreId, $columns)
    {
        try {
            return Screen::where('theatre_id', $theatreId)->limit(5)->get($columns);
        } catch(Exception $e) {
            Log::error(
                'Error in getScreen method of Screen model: '
                . $e->getMessage());
            abort(500);
        }
    }

    /**
     * Fetches a screen record with the given theatre's id.
     *
     * @param integer $id     theatre id.
     * @param array $columns  Array of all the columns required.
     *
     * @return mixed
     */
    public static function getScreensUsingTheatreId($id, $columns)
    {
        try {
            return Screen::where('theatre_id', $id)->limit(5)->get($columns);
        } catch(Exception $e) {
            Log::error(
                'Error in getScreensUsingTheatreId method of Screen model: '
                . $e->getMessage());
            abort(500);
        }
    }
}
