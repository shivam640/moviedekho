<?php
/**
 * This model provides methods to manage bookings.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Config;

/**
 * Class Booking
 *
 * @package App
 */
class Booking extends Model
{
    /**
     * Adding booking details to the bookings table.
     *
     * @param Request $request  Instance of the current HTTP request.
     *
     * @return void
     */
    public static function store($request)
    {

        $total = 0;
        /** Calculating the total amount. */
        $total = count($request->seats) * Config::get('constants.COST');
        /** Adding 30 rs for internet handling fees. */
        $total += Config::get('constants.INTERNET_FEES');
        try {
            $booking = new Booking;
            $booking->user_id = Auth::id();
            $booking->show_id = $request->showId;
            $booking->seats = json_encode($request->seats);
            $booking->amount = $total;
            $booking->save();

            return $booking->id;
        } catch(Exception $e) {
            Log::error(
                'Error in store method of Booking model: '
                . $e->getMessage());
        }
    }

    /**
     * Updating the paid column of the bookings table.
     *
     * @param integer $id  Booking id.
     *
     * @return void
     */
    public static function updatePayment($id)
    {
        try {
            $booking = Booking::find($id);
            $booking->paid = 1;
            $booking->save();
        } catch (Exception $e) {
            Log::error(
                'Error in update method of Booking model: '
                . $e->getMessage());
        }
    }

    /**
     * Deleting the unpaid bookings.
     *
     * @return void
     */
    public static function deleteUnpaidBooking()
    {
        try {
            Booking::where([
                ['created_at', '<', Carbon::now()->subMinutes(10)],
                ['paid', '=', '0'],
            ])->delete();
        } catch (Exception $e) {
            Log::error(
                'Error in deleteUnpaidBooking method of Booking model: '
                . $e->getMessage());
        }
    }

    /**
     * Fetches a booking record for a given id.
     *
     * @param string $id Booking id.
     * @param array $columns Array containing columns required.
     *
     * @return mixed
     */
    public static function getBooking($id, $columns)
    {
        try {
            return Booking::select($columns)->find($id);
        } catch (Exception $e) {
            Log::error(
                'Error in getBooking method of Booking model: '
                . $e->getMessage());
        }
    }
}
