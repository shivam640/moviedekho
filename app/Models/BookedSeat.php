<?php
/**
 * This model provides methods to update seats.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

/**
 * Class BookedSeat
 *
 * @package App
 */
class BookedSeat extends Model
{
    protected $fillable = [
        'show_id', 'booked_seats'
    ];

    /**
     * Updating the newly booked seats.
     *
     * @param integer $showId  Show id of selected show.
     * @param string $seats     Seats selected.
     *
     * @return void
     */
    public static function updateSeats($showId, $seats)
    {
        try {
            $bookedSeatsObject = BookedSeat::where(
                'show_id', $showId)
                ->select('booked_seats', 'show_id', 'id')->first();
            $bookedSeatsArray = [];
            if (null !== $bookedSeatsObject &&
                isset($bookedSeatsObject->booked_seats)) {
                $bookedSeatsArray = json_decode(
                    $bookedSeatsObject->booked_seats
                );
            }
            if (isset($seats)) {
                $newBookedSeatArray = json_decode($seats);
                foreach ($newBookedSeatArray as $seat) {
                    array_push($bookedSeatsArray, $seat);
                }
            }
            $seatJSON = json_encode($bookedSeatsArray);

            BookedSeat::updateOrCreate(
                ['show_id' => $showId],
                ['booked_seats' => $seatJSON]
            );
        } catch(Exception $e) {
            Log::error(
                'Error in updateSeats method of BookedSeat model: '
                . $e->getMessage());
        }
    }

    /**
     * Getting the booked seats of a particular show.
     *
     * @param string $showId  Show id of the selected show.
     * @param array $columns  Array containinf the required feilds.
     *
     * @return object
     */
    public static function getBookedSeats($showId, $columns)
    {
        try {
            return BookedSeat::where('show_id', $showId)
                ->first($columns);
        } catch(Exception $e) {
            Log::error(
                'Error in getBookedSeats method of BookedSeat model: '
                . $e->getMessage());
            abort(500);
        }
    }
}
