<?php
/**
 * This model provides methods to store and edit theatres.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class Theatre
 * @package App
 */
class Theatre extends Model
{

    /**
     * @var $alias_fields Array List of column alias.
     */
    protected $alias_fields = [
        'theatres.theatre_name', 'theatres.address', 'theatres.city', 'theatres.state',
        'group_concat(screens.Screen_name,"(",screens.capacity,")" SEPARATOR ", ")'
    ];
    /**
     * The attributes that are used display.
     *
     * @var array
     */
    protected $columns = [
        'theatre_name', 'address', 'city', 'state', 'screens'
    ];

    /**
     * Storing record to theatres table.
     *
     * @param Request $request  Instance of the current HTTP request.
     *
     * @return mixed
     */
    public static function store($request)
    {
        try {
            $theatre = new Theatre;
            $theatre->theatre_name = $request->theatreName;
            $theatre->address = $request->address;
            $theatre->city = $request->city;
            $theatre->state = $request->state;
            $theatre->save();

            return $theatre->id;
        } catch(Exception $e) {
            Log::error(
                'Error in store method of Theatre model: '
                . $e->getMessage());

            return false;
        }
    }

    /**
     * Updating theatres record.
     *
     * @param Request $request  Instance of the current HTTP request.
     *
     * @return bool
     */
    public static function edit($request)
    {
        try {
            $theatre = Theatre::find($request->theatreId);
            $theatre->theatre_name = $request->theatreName;
            $theatre->address = $request->address;
            $theatre->city = $request->city;
            $theatre->state = $request->state;

            return $theatre->update();
        } catch(Exception $e) {
            Log::error(
                'Error in edit method of Theatre model: '
                . $e->getMessage());

            return false;
        }
    }

    /**
     * Fetches all theatres records.
     *
     * @param array $columns  Array of all the columns required.
     *
     * @return mixed
     */
    public static function getAllTheatres($columns)
    {
        try {
            return Theatre::get($columns);
        } catch(Exception $e) {
            Log::error(
                'Error in getAllTheatres method of Theatre model: '
                . $e->getMessage());
            abort(500);
        }
    }

    public static function getTheatre($id ,$columns)
    {
        try {
            return Theatre::select($columns)->find($id);
        } catch(Exception $e) {
            Log::error(
                'Error in getAllTheatres method of Theatre model: '
                . $e->getMessage());
            abort(500);
        }
    }

    /**
     * Delete the theatre for the given id.
     *
     * @param string $id  Id of the theatre to be deleted.
     *
     * @return bool|int
     */
    public static function theatreDelete($id)
    {
        try {
            return Theatre::destroy($id);
        } catch(Exception $e) {
            Log::error(
                'Error in theatreDelete method of Theatre model: '
                . $e->getMessage());

            return false;
        }
    }

    /**
     * Function to get column alias
     *
     * @return Array
     */
    public function getTableAliasColumns()
    {
        return $this->alias_fields;
    }

    /**
     * Get table columns for data tables.
     *
     * @return array
     */
    public function getTableColumns()
    {
        return $this->columns;
    }

    /**
     * Returns data to for the data table.
     *
     * @return mixed
     */
    public function getModelData()
    {
        return DB::table('theatres')
            ->select('theatres.id as DT_RowId', 'theatres.theatre_name'
                , 'theatres.address' , 'theatres.city', 'theatres.state',
                DB::raw(
                    'group_concat(screens.Screen_name,"(",screens.capacity,")"
                     SEPARATOR ", ") as screens'
                ))
            ->join('screens', 'screens.theatre_id', '=', 'theatres.id')
            ->groupBy('screens.theatre_id');
    }
}
